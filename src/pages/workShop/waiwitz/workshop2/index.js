import BannerHome from "src/components/waiwitz/workshop2/bannerHome";
import Navbar from "/src/components/waiwitz/workshop2/navBar";
import IntroBox from "src/components/waiwitz/workshop2/introBox";
import Footer from "src/components/waiwitz/workshop2/footer";
import Introduce1 from "src/components/waiwitz/workshop2/introduce1";
import { Container } from "@mui/system";
import { Avatar, Box, Button, Grid, Paper, Typography } from "@mui/material";
import styles from "src/styles/waiwitzStyles.module.css";
import ForumIcon from '@mui/icons-material/Forum';
import DesignServicesIcon from '@mui/icons-material/DesignServices';
import HomeIcon from '@mui/icons-material/Home';
import Introduce2 from "src/components/waiwitz/workshop2/introduce2";
import Introduce3 from "src/components/waiwitz/workshop2/introduce3";
import Introduce4 from "src/components/waiwitz/workshop2/introduce4";
import BackButton from "src/components/waiwitz/backButton";

const introboxData = [
    {
        title: 'HOME DESIGN CONSULTATION',
        detail: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
    },
    {
        title: 'HOME DESIGN 3D 2D INTERIOR SERVICE',
        detail: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
    },
    {
        title: 'HOME DESIGN CONSULTATION',
        detail: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.',
    }
];

const findOdd = (index) => {
    let cal = index % 2
    return cal;
}


const workShop2 = () => {
    return (
        <>
            <BackButton/>
            <Navbar />
            <BannerHome />
            <Box sx={{ display: 'flex', flexWrap: 'wrap', justifyContent: 'center', transform: 'translateY(-100px)' }}>
                {introboxData.map((item, index) => (
                    <IntroBox title={item.title} detail={item.detail} color={findOdd(index) !== 0 ? '#787474' : '#201414'} />
                ))}
            </Box>
            <Container maxWidth='xl'>
                <Introduce1 head1={'WHO WE ARE'} head2={'WE ARE PERFECT TEAM FOR HOME INTERIOR DECORATION'} body={'Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo'} />
                <Box sx={{ marginTop: '5em' }}>
                    <Typography gutterBottom>HOW WE WORK</Typography>
                    <Typography variant="h3" sx={{ fontWeight: 'bold', marginBottom: '1.5em' }} >OUR WORK PROCEDURE</Typography>
                    <Grid container columns={{ xs: 4, sm: 8, md: 12 }} spacing={4}>
                        <Grid item xs={4}>
                            <Paper sx={{ border: '1px solid #cfcfcf', padding: '2em 1.5em', borderRadius: '0' }}>
                                <ForumIcon sx={{ fontSize: '45px', color: '#757575', marginBottom: '20px' }} />
                                <Typography variant="h5" sx={{ fontWeight: 'bold' }} gutterBottom>CLIENT DESIGN CONSULTATION</Typography>
                                <Typography variant="body1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={4}>
                            <Paper sx={{ border: '1px solid #cfcfcf', padding: '2em 1.5em', borderRadius: '0', background: '#757575' }}>
                                <DesignServicesIcon sx={{ fontSize: '45px', color: '#eee', marginBottom: '20px' }} />
                                <Typography variant="h5" sx={{ fontWeight: 'bold' }} color={'white'} gutterBottom>PROTOTYPING HOME DESIGN</Typography>
                                <Typography variant="body1" color={'white'}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</Typography>
                            </Paper>
                        </Grid>
                        <Grid item xs={4}>
                            <Paper sx={{ border: '1px solid #cfcfcf', padding: '2em 1.5em', borderRadius: '0' }}>
                                <HomeIcon sx={{ fontSize: '45px', color: '#757575', marginBottom: '20px' }} />
                                <Typography variant="h5" sx={{ fontWeight: 'bold' }} gutterBottom>PROCESSING TO DESIGN HOME</Typography>
                                <Typography variant="body1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</Typography>
                            </Paper>
                        </Grid>
                    </Grid>
                </Box>
            </Container>
            <Box sx={{ margin: '5em 0' }}>
                <Grid container columns={{ xs: 4, sm: 8, md: 12 }} spacing={4} wrap="nowrap">
                    {Array.from(Array(5), (i) => {
                        return <Grid item xs={3}>
                            <Box sx={{ background: '#C4C4C4', height: '250px' }}></Box>
                        </Grid>
                    })}
                </Grid>
            </Box>
            <Container maxWidth='xl'>
                <Introduce2 head1={'PERFECT PARTNER'} head2={'WE HAVE PRIORITY FOR CAN CREATE DREAM HOME DESIGN'} body={`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.`} />
                <Introduce3 head1={'TRUST US NOW'} head2={'WHY CHOICE OUR HOME DESIGN INTERIOR SERVICES'} body={`Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.`} />
                <Introduce4 head1={'CLIENTS FEEDBACK'} head2={'OUR TESTIMONIAL FROM BEST CLIENTS'} body={`Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.`} />

            </Container>
            <Footer />
        </>
    )
};

export default workShop2;
