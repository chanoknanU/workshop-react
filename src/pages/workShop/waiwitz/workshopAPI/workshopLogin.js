import { Box, Breadcrumbs, Button, Container, FormControl, OutlinedInput, TextField, Typography } from "@mui/material";
import Head from "next/head";
import { useRef, useState } from "react";
import ArrowBackIosIcon from '@mui/icons-material/ArrowBackIos';
import Link from "next/link";

const Login = async (user) => {
    try {
        const data = await fetch(`${process.env.NEXT_PUBLIC_API_LOGIN_URL}`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify(user)
        });
        return await data.json();
    } catch (error) {
        console.log(error)
    }
}


const signin = () => {
    const [username, setUsername] = useState('karn.yong@melivecode.com');
    const [password, setPassword] = useState('melivecode');
    const [validUsername, setValidUsername] = useState(false);
    const [validPassword, setvalidPassword] = useState(false);
    const [helperTextUsername, setHelperTextUsername] = useState();
    const [helperTextPassword, setHelperTextPassword] = useState();

    const handleSubmit = async (event) => {
        event.preventDefault();
        const res = await Login({
            username, password
        });
        try {
            username ? setValidUsername(false) & setHelperTextUsername('') : ''
            password ? setvalidPassword(false) & setHelperTextPassword('') : ''
            console.log(res);
            if ('accessToken' in res) {
                localStorage.setItem('Token', res['accessToken']);
                // localStorage.setItem('User', JSON.stringify(res['user']));
                console.info('%cเข้าสู่ระบบสำเร็จ', 'background: linear-gradient(0deg, rgba(132,34,195,1) 0%, rgba(0,3,255,1) 16%, rgba(103,132,159,1) 29%, rgba(124,190,116,1) 44%, rgba(233,253,45,1) 62%, rgba(253,165,45,1) 77%, rgba(253,45,45,1) 100%);');
                console.log(`Token : ${localStorage.getItem('Token')}`)
                alert(`เข้าสู่ระบบสำเร็จ`)
                window.location.href = '/workShop/waiwitz/workshopAPI/workshopUser'
                // console.log
            } else {
                console.error(`status : ${res.status}`);
                console.info(`info : ${res.message}`);
                !username ? setValidUsername(true) & setHelperTextUsername('กรุณากรอกชื่อผู้ใช้') : ''
                !password ? setvalidPassword(true) & setHelperTextPassword('กรุณากรอกรหัสผ่าน') : ''
                if (!username || !password) {
                    console.info(`ช่องชื่อผู้ใช้หรือรหัสผ่านว่าง `);
                    console.info(`username : '${username}', password : '${password}'`);
                } else if (username != res.username || password != res.password) {
                    console.info(res.message)
                    alert('ชื่อผู้ใช้หรือรหัสผ่านไม่ตรง')
                    // console.info(`username : ${username == res.username ? 'ตรง' : 'ไม่ตรง'}, password : ${res.password == password ? 'ตรง' : 'ไม่ตรง'}`)
                }

            }
        } catch (error) {
            console.error(error);
            alert(error)
        }
    }

    const token = localStorage.getItem('Token');
    if (token) {
        window.location.href = '/workShop/waiwitz/workshopAPI/workshopUser'
    }
    return (
        <>
            <Head>
                <title>Workshop 3 || Login API</title>
            </Head>
            <Container maxWidth='sm' sx={{ marginTop: '10em' }}>
                <Link href='/workShop/waiwitz'>
                    <Button sx={{ bgcolor: '#f5f5f5' }}>
                        <ArrowBackIosIcon></ArrowBackIosIcon> กลับไปหน้า workShop
                    </Button>
                </Link>
                <Typography variant="h4" marginTop={3}>Workshop 3 || Login API</Typography>
                <Breadcrumbs maxItems={3} aria-label="breadcrumb" style={{ margin: '10px 0' }}>
                    <Link underline="hover" color="inherit" href="/workShop">
                        WorkShop
                    </Link>
                    <Link underline="hover" color="inherit" href="/workShop/waiwitz">
                        การ์ด
                    </Link>
                    <Typography color="text.primary">Workshop 3 </Typography>
                </Breadcrumbs>
                <Box margin={'auto'} border={'solid 1px #dcdcdc'} borderRadius={2} padding={5} boxShadow={5}>
                    <Typography variant="h3" gutterBottom>Login</Typography>
                    {/* {localStorage.getItem('accessToken')} */}
                    <form noValidate autoComplete="off" onSubmit={handleSubmit}>
                        <div>
                            <TextField error={validUsername} helperText={helperTextUsername} type="text"
                                label="Username"
                                defaultValue={'karn.yong@melivecode.com'}
                                sx={{ width: '100%', marginBottom: '1em' }}
                                onChange={(event) => setUsername(event.target.value)} />
                            <TextField error={validPassword} helperText={helperTextPassword} label="Password" sx={{ width: '100%' }}
                                defaultValue={'melivecode'} type="password"
                                onChange={(event) => setPassword(event.target.value)} />
                        </div>
                        <div style={{ textAlign: 'end' }}>
                            <Button type="submit" variant="outlined" sx={{ marginTop: '10px' }} >เข้าสู่ระบบ</Button>
                        </div>
                    </form>
                </Box>
            </Container>
        </>
    )
}

export default signin;