import ResponsiveAppBar from "src/components/freshy/designUI/appBar";

export default function About() {
    return (
        <>
            <ResponsiveAppBar currentPage='About us'/>
        </>
    );
}
