import { Box, Container, Paper } from "@mui/material";
import ResponsiveAppBar from "src/components/freshy/designUI/appBar";
import Campaign from "src/components/freshy/designUI/campaign";
import Carousel from "src/components/freshy/designUI/carousel";
import Feedback from "src/components/freshy/designUI/feedback";
import FooterSection from "src/components/freshy/designUI/footer";
import HeroSection from "src/components/freshy/designUI/hero";
import Introduction from "src/components/freshy/designUI/introduction";
import PartnerSection from "src/components/freshy/designUI/partner";
import Procedure from "src/components/freshy/designUI/procedure";
import TrustSection from "src/components/freshy/designUI/trust";

export default function index() {
    return (
        <>
            <ResponsiveAppBar currentPage='Home' />
            <Paper elevation={0} sx={{ position: 'relative', overflow: 'hidden' }}>
                <Container maxWidth='xl'>
                    <HeroSection />
                    <Campaign />
                    <Introduction />
                    <Procedure />
                </Container>
                <Carousel />
                <Container maxWidth='xl'>
                    <PartnerSection />
                    <TrustSection />
                    <Feedback />
                </Container>
                <FooterSection />
            </Paper>
        </>
    );
}
