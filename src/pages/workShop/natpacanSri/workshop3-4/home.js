import React, { useEffect, useState } from 'react';
import Image from 'next/image'
import { CircularProgress, Typography, Paper, Avatar, Button, ButtonBase } from '@mui/material';
import { styled } from '@mui/system';

const Container = styled('div')({
  padding: '20px',
  height: "100vh",
  display: "flex",
});

const UserInfoContainer = styled('div')({
  margin: 'auto',
  // marginTop: '20px',
  border: "1px solid #e5e7eb",
  boxShadow: 3,
  padding: '30px',
  borderRadius: '15px'
});

const UserAvatar = styled(Avatar)({
  width: '300px',
  height: '300px',
  margin: '20px auto',
});


const Home = () => {
  // สร้าง state เพื่อเก็บข้อมูลผู้ใช้
  const [userData, setUserData] = useState('loading...');

  useEffect(() => {
    // ดึง JWT จาก local storage
    const token = localStorage.getItem('jwt');

    // ถ้ามี JWT ให้ทำการดึงข้อมูลผู้ใช้
    if (token) {
      console.log("token:", token);
      fetch(`${process.env.NEXT_PUBLIC_API_URL}${process.env.NEXT_PUBLIC_USER_EXIST}`, {
        method: 'GET',
        headers: {
          'Authorization': `Bearer ${token}`
        },
      })
        .then(response => response.json())
        .then(data => {
          // เก็บข้อมูลผู้ใช้ลง state
          setUserData(data);

          // เก็บข้อมูลผู้ใช้ลง local storage
          localStorage.setItem('userData', JSON.stringify(data));
        })
        .catch(error => console.error('Error fetching user data:', error));

    } else {
      console.warn('dont have token!!?');
    }
  }, []); // ทำการดึงข้อมูลเมื่อ component ถูก render ครั้งแรกเท่านั้น

  console.info('userData:', userData); // Log the entire user data object

  return (
    <Container>
      {userData?.user ? (
          <UserInfoContainer sx={{ boxShadow: 3 }}>
            <Typography variant="h2">User Information</Typography>
            <UserAvatar src={userData?.user?.avatar} alt="User Avatar" />
            <Typography>ID: {userData?.user?.id}</Typography>
            <Typography>First Name: {userData?.user?.fname}</Typography>
            <Typography>Last Name: {userData?.user?.lname}</Typography>
            <Typography>Username: {userData?.user?.username}</Typography>
            <Typography>Email: {userData?.user?.email}</Typography>
            {/* Password is sensitive information and should not be displayed */}
            {/* Add more Typography components for other data */}
          <Button variant='contained' color='primary' fullWidth sx={{mt: 2}}>
            test
          </Button>
          </UserInfoContainer>
      ) : (
        <CircularProgress
          sx={{
            margin: 'auto'
          }}
        />
      )}
    </Container>
  );
}

export default Home;
