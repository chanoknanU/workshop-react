import { Inter } from 'next/font/google'
import { Main } from 'next/document'
import Container from '@mui/material/Container';

// import myComponents
import Navbar from 'src/components/johnny/resume/_navbar'
import ProfileName from 'src/components/johnny/resume/profileName'
import CareerObj from 'src/components/johnny/resume/careerObj'
import SkillProject from 'src/components/johnny/resume/skillProject'
import Info from 'src/components/johnny/resume/info'


export default function Home() {
  return (
    <Container maxWidth="lg">
      <Navbar/>
      <ProfileName/>
      <Info/>
      <SkillProject/>
      <CareerObj/>
    </Container>
  )
}
