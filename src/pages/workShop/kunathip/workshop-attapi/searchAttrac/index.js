// Import libraries and components
import React, { useEffect, useState } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import Card from "@mui/material/Card";
import CardContent from "@mui/material/CardContent";
import CardMedia from "@mui/material/CardMedia";
import Grid from "@mui/material/Grid";
import Navbar from "src/components/Por/Navbar";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";


const AttractionsPage = () => {
  const [attractions, setAttractions] = useState([]);
  const [search, setSearch] = useState("");
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);

  useEffect(() => {
    fetchAttractions();
  }, [currentPage]);

  const handleSearch = () => {
    setCurrentPage(1);
    fetchAttractions();
  };

  const fetchAttractions = () => {
    fetch(
      `${process.env.NEXT_PUBLIC_API_URL}/th/attractions?language=en&search=${search}&page=${currentPage}&per_page=10&sort_column=id&sort_order=desc`
    )
      .then((response) => response.json())
      .then((data) => {
        console.log("Attractions data received:", data);
        setAttractions(data.data);
        setTotalPages(data.total_pages);
      })
      .catch((error) => {
        console.error("Error fetching attractions:", error);
      });
  };

  const handleNextPage = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
    }
  };

  const handlePreviousPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
    }
  };

  return (
    <React.Fragment>
      <Navbar />
      <CssBaseline />
      <Container maxWidth="lg" sx={{ mt: 4, mb: 4 }}>
        <Typography variant="h4" component="div" gutterBottom>
          Attractions
        </Typography>
        <div style={{ display: "flex", alignItems: "center", marginBottom: 2 }}>
          <TextField
            label="Search"
            variant="outlined"
            fullWidth
            value={search}
            onChange={(e) => setSearch(e.target.value)}
            sx={{ marginRight: 1, width: "20%" }}
          />
          <Button
            variant="contained"
            onClick={handleSearch}
            sx={{ backgroundColor: "#19a7ce", color: "white" }}
          >
            Search
          </Button>
        </div>

        <Grid marginTop={1} container spacing={3}>
          {attractions.map((attraction) => (
            <Grid item key={attraction.id} xs={12} sm={6} md={4}>
              <Card>
                <CardMedia
                  component="img"
                  alt={attraction.name}
                  height="140"
                  image={attraction.coverimage}
                />
                <CardContent>
                  <Typography variant="h6" component="div">
                    {attraction.name}
                  </Typography>
                  <Typography variant="body2" color="text.secondary">
                    {attraction.detail}
                  </Typography>
                </CardContent>
              </Card>
            </Grid>
          ))}
        </Grid>

        <div style={{ marginTop: "3rem" }}>
          {currentPage > 1 && (
            <Button
              variant="contained"
              onClick={handlePreviousPage}
              sx={{ marginRight: 2, backgroundColor: "#19a7ce", color: "white" }}
            >
              Previous Page
            </Button>
          )}

          {currentPage < totalPages && (
            <Button
              variant="contained"
              onClick={handleNextPage}
              sx={{ backgroundColor: "#19a7ce", color: "white" }}
            >
              Next Page
            </Button>
          )}
        </div>
      </Container>
    </React.Fragment>
  );
};

export default AttractionsPage;
