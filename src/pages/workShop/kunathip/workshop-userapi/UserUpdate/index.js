import React, { useState, useEffect } from "react";
import CssBaseline from "@mui/material/CssBaseline";
import Container from "@mui/material/Container";
import Typography from "@mui/material/Typography";
import TextField from "@mui/material/TextField";
import Button from "@mui/material/Button";
import Select from "@mui/material/Select";
import MenuItem from "@mui/material/MenuItem";
import { useRouter } from "next/router";

export default function UserUpdate() {
  const [userData, setUserData] = useState({
    id: null,
    fname: "",
    lname: "",
    username: "",
    email: "",
    avatar: "",
  });

  const [users, setUsers] = useState([]);
  const [selectedUserId, setSelectedUserId] = useState(null);

  const router = useRouter();
  const { id } = router.query;

  useEffect(() => {
    if (id) {
      // Fetch user data for the specified ID
      fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${id}`)
        .then((res) => res.json())
        .then((result) => {
          console.log("User data for update:", result);
          setUserData(result);
        })
        .catch((error) => {
          console.log("Error fetching user data for update:", error);
        });
    }
  }, [id]);

  useEffect(() => {
    // Fetch users for the dropdown
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users`)
      .then((res) => res.json())
      .then((result) => {
        console.log("Users data:", result);
        setUsers(result);
      })
      .catch((error) => {
        console.log("Error fetching users data:", error);
      });
  }, []); // Empty dependency array to fetch users only once on component mount

  const handleUserSelect = (event) => {
    const selectedId = event.target.value;
    setSelectedUserId(selectedId);
    fetchUserData(selectedId);
  };

  const fetchUserData = (userId) => {
    // Fetch user data for the specified ID
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/${userId}`)
      .then((res) => res.json())
      .then((result) => {
        console.log("User data for update:", result);
        setUserData(result);
      })
      .catch((error) => {
        console.log("Error fetching user data for update:", error);
      });
  };

  const handleUpdate = () => {
    console.log("Updating user with ID:", id);

    // Prepare the request body
    const requestBody = {
      id: id,
      fname: userData.fname,
      lname: userData.lname,
      username: userData.username,
      email: userData.email,
      avatar: userData.avatar,
    };

    // Send a PUT request to update user data
    fetch(`${process.env.NEXT_PUBLIC_API_URL}/users/update`, {
      method: "PUT",
      headers: {
        "Content-Type": "application/json",
      },
      body: JSON.stringify(requestBody),
    })
      .then((response) => response.json())
      .then((result) => {
        console.log("Update User Result:", result);

        if (result.status === "ok") {
          console.log("User updated successfully");
          // Handle success, e.g., show a success message
          alert(result.message);
        } else {
          console.log("User update failed");
          // Handle failure, e.g., show an error message
          alert(result.message);
        }
      })
      .catch((error) => {
        console.error("Update User Error:", error);
        // Handle error, e.g., show an error message
        alert("Failed to update user");
      });
  };

  return (
    <React.Fragment>
      <CssBaseline />
      <Container maxWidth="sm" sx={{ p: 2 }}>
        <Typography variant="h6" gutterBottom component="div">
          Update User
        </Typography>
        <Select label="Select User" value={selectedUserId || ""} onChange={handleUserSelect}>
          {users.map((user) => (
            <MenuItem key={user.id} value={user.id}>
              {user.fname} {user.lname}
            </MenuItem>
          ))}
        </Select>
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="fname"
          label="First Name"
          name="fname"
          value={userData.fname}
          onChange={(e) => setUserData({ ...userData, fname: e.target.value })}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="lname"
          label="Last Name"
          name="lname"
          value={userData.lname}
          onChange={(e) => setUserData({ ...userData, lname: e.target.value })}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="username"
          label="Username"
          name="username"
          value={userData.username}
          onChange={(e) => setUserData({ ...userData, username: e.target.value })}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="email"
          label="Email"
          name="email"
          value={userData.email}
          onChange={(e) => setUserData({ ...userData, email: e.target.value })}
        />
        <TextField
          variant="outlined"
          margin="normal"
          required
          fullWidth
          id="avatar"
          label="Avatar"
          name="avatar"
          value={userData.avatar}
          onChange={(e) => setUserData({ ...userData, avatar: e.target.value })}
        />
        <Button fullWidth variant="contained" sx={{ mt: 3, mb: 2 }} onClick={handleUpdate}>
          Update
        </Button>
      </Container>
    </React.Fragment>
  );
}
