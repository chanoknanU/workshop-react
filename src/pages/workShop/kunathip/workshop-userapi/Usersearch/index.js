import React, { useState, useEffect } from "react";
import { TextField, Button, Grid, Card, CardContent, CardMedia, Typography } from "@mui/material";
import styled from "@emotion/styled";
import Navbar from "src/components/Por/Navbar";

const SearchContainer = styled.div`
  padding: 20px;
`;

const UserCard = styled(Card)`
  margin: 10px;
  cursor: pointer;
`;

const UserSearchPage = () => {
  const [searchTerm, setSearchTerm] = useState("");
  const [users, setUsers] = useState([]);
  const handleSearch = async () => {
    try {
      const apiUrl = `${process.env.NEXT_PUBLIC_API_URL}/users?search=${searchTerm}`;
      const response = await fetch(apiUrl);
      const data = await response.json();

      if (data && data.length > 0) {
        setUsers(data);
        console.log("Success user search:", data);
      } else {
        console.log("No users found for the search term:", searchTerm);
      }
    } catch (error) {
      console.error("Error fetching users:", error);
    }
  };

  useEffect(() => {
    handleSearch();
  }, []);

  const handleCardClick = (userId) => {
    window.location.href = `/user/${userId}`;
  };

  return (
    <div>
      <Navbar />
      <SearchContainer>
        <TextField
          label="Search Users"
          variant="outlined"
          value={searchTerm}
          onChange={(e) => setSearchTerm(e.target.value)}
        />
        <Button variant="contained" onClick={handleSearch}>
          Search
        </Button>
      </SearchContainer>

      <Grid container spacing={2}>
        {users.map((user) => (
          <Grid item key={user.id} xs={12} sm={6} md={4} lg={3}>
            <UserCard onClick={() => handleCardClick(user.id)}>
              <CardMedia
                component="img"
                height="140"
                image={user.avatar}
                alt={`${user.fname} ${user.lname}`}
              />
              <CardContent>
                <Typography variant="h6">{`${user.fname} ${user.lname}`}</Typography>
                <Typography variant="body2">{user.username}</Typography>
              </CardContent>
            </UserCard>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default UserSearchPage;
