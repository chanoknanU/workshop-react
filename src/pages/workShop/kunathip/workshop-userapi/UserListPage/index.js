import React, { useState, useEffect } from "react";
import { TextField, Button, Grid, Card, CardContent, CardMedia, Typography } from "@mui/material";
import styled from "@emotion/styled";
import Navbar from "src/components/Por/Navbar";
const SearchContainer = styled.div`
  padding: 20px;
  display: flex;
  align-items: center;
  justify-content: space-between;

  & > div {
    display: flex;
    gap: 10px;
    align-items: center;
  }
`;
const UserCard = styled(Card)`
  margin: 10px;
  cursor: pointer;
  display: flex;
  flex-direction: column;
  align-items: center;
`;

const NavigationButtons = styled.div`
  display: flex;
  gap: 10px;
`;

const UserListPage = () => {
  const [users, setUsers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  const [totalPages, setTotalPages] = useState(1);
  const [searchTerm, setSearchTerm] = useState("");

  const fetchUsers = async (page, search = "") => {
    try {
      const apiUrl = `${process.env.NEXT_PUBLIC_API_URL}/users?page=${page}&per_page=10&search=${search}`;
      const response = await fetch(apiUrl);
      const data = await response.json();

      if (data && Array.isArray(data.data)) {
        setUsers(data.data);
        setTotalPages(data.total_pages);
        console.log("Fetched users successfully:", data);
      }
    } catch (error) {
      console.error("Error fetching users:", error);
    }
  };

  const handleNextPage = () => {
    if (currentPage < totalPages) {
      setCurrentPage(currentPage + 1);
      fetchUsers(currentPage + 1, searchTerm);
      console.log("Navigated to next page");
    }
  };

  const handlePrevPage = () => {
    if (currentPage > 1) {
      setCurrentPage(currentPage - 1);
      fetchUsers(currentPage - 1, searchTerm);
      console.log("Navigated to previous page");
    }
  };

  const handleSearch = () => {
    fetchUsers(1, searchTerm);
    setCurrentPage(1);
    console.log("Initiated user search with term:", searchTerm);
  };

  const handleCardClick = (userId) => {
    window.location.href = `/user/${userId}`;
  };

  useEffect(() => {
    fetchUsers(currentPage, searchTerm);
    console.log("Fetched users on component mount");
  }, [currentPage, searchTerm]);

  return (
    <div>
      <Navbar />
      <SearchContainer>
        <div>
          <TextField
            label="Search Users"
            variant="outlined"
            value={searchTerm}
            onChange={(e) => setSearchTerm(e.target.value)}
          />
          <Button
            variant="contained"
            onClick={handleSearch}
            sx={{ backgroundColor: "#2196F3", color: "white" }}
          >
            Search
          </Button>
        </div>
        <NavigationButtons>
          <Button
            variant="contained"
            onClick={handlePrevPage}
            disabled={currentPage === 1}
            sx={{ backgroundColor: "#2196F3", color: "white" }}
          >
            Previous Page
          </Button>
          <Button
            variant="contained"
            onClick={handleNextPage}
            disabled={currentPage === totalPages}
            sx={{ backgroundColor: "#2196F3", color: "white" }}
          >
            Next Page
          </Button>
        </NavigationButtons>
      </SearchContainer>

      <Grid container spacing={2}>
        {users.map((user) => (
          <Grid item key={user.id} xs={12} sm={6} md={4} lg={3}>
            <UserCard onClick={() => handleCardClick(user.id)}>
              <CardMedia
                component="img"
                style={{ width: "50%", height: "50%" ,marginTop: '1rem'}}
                image={user.avatar}
                alt={`${user.fname} ${user.lname}`}
              />

              <CardContent>
                <Typography variant="h6">{`${user.fname} ${user.lname}`}</Typography>
                <Typography variant="body2">{user.username}</Typography>
              </CardContent>
            </UserCard>
          </Grid>
        ))}
      </Grid>
    </div>
  );
};

export default UserListPage;
