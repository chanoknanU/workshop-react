import { useState } from 'react';
import axios from 'axios';
import { useRouter } from 'next/router';
import { Container, Typography, TextField, Button, Paper, createTheme, ThemeProvider } from '@mui/material';
import Navbar from 'src/components/Por/Navbar';
const Login = () => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const router = useRouter();

  const handleLogin = async () => {
    try {
      const response = await axios.post(`${process.env.NEXT_PUBLIC_API_URL}/login`, {
        username,
        password,
      });

      console.log('Response from server:', response.data);

      if (response.data.status === 'ok') {
        localStorage.setItem('accessToken', response.data.accessToken);
        console.log('Login successful!');
        console.log('User data:', response.data.user);
        router.push('/workShop/kunathip/workshop-login/user');
      } else {
        console.error('Login failed:', response.data.message);
      }
    } catch (error) {
      console.error('API login error:', error);
    }
  };

  const theme = createTheme({
    palette: {
      primary: {
        main: '#19a7ce',
      },
    },
  });

  return (
    <>
    <Navbar/>
    <ThemeProvider theme={theme}>
      <Container component="main" maxWidth="xs">
        <Paper elevation={3} style={{ padding: 20, display: 'flex', flexDirection: 'column', alignItems: 'center', marginTop: '10rem' }}>
          <Typography component="h1" variant="h5" gutterBottom>
            Login
          </Typography>
          <TextField
            label="Username"
            variant="outlined"
            margin="normal"
            fullWidth
            value={username}
            onChange={(e) => setUsername(e.target.value)}
          />
          <TextField
            label="Password"
            variant="outlined"
            margin="normal"
            type="password"
            fullWidth
            value={password}
            onChange={(e) => setPassword(e.target.value)}
          />
          <Button
            variant="contained"
            color="primary"
            fullWidth
            style={{ marginTop: 20 ,color:'white',fontWeight: 'bold'}}
            onClick={handleLogin}
          >
            Login
          </Button>
        </Paper>
      </Container>
    </ThemeProvider>
    </>
  );
};

export default Login;
