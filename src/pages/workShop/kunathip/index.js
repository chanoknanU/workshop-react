import { Layout } from "src/layouts/dashboard/layout";
import { styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Image from "next/image";
import Link from "next/link";
import Button from "@mui/material/Button";

const Item = styled(Paper)(({ theme }) => ({
  backgroundColor: theme.palette.mode === "dark" ? "#1A2027" : "#fff",
  ...theme.typography.body2,
  padding: theme.spacing(1),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const getImagePath = (imageName) => {
  return `/imageknt/${imageName}`;
};

const kunathip = () => {
  const profileImagePath = "https://i.ibb.co/b1BhzQ8/studentphoto.png";

  const fontSize1 = {
    color: "#FFFFFF",
    textAlign: "left",
    fontSize: "25px",
    fontWeight: "bold",
  };

  const fontSize2 = {
    color: "#FFFFFF",
    textAlign: "left",
    fontSize: "40px",
    fontWeight: "bold",
  };

  const fontSize3 = {
    color: "#FFFFFF",
    textAlign: "left",
    fontSize: "15px",
  };

  const textWorkshop = {
    color: "#FFFFFF",
    textAlign: "left",
    fontSize: "30px",
    marginTop: "2em",
    fontWeight: "bold",
    textAlign: "left",
  };

  const buttonNext = {
    marginTop: "1em",
    cursor: "pointer",
    whiteSpace: "nowrap",
  };

  const imageProfile = {
    margin: "10px",
    borderRadius: "50%", // Make the image circular
    boxShadow: "5px 10px 450px #6366F1",
    overflow: "hidden",
    width: "auto", // Adjusted to be responsive
    height: "auto", // Adjusted to be responsive
    maxWidth: "350px", // Added for a maximum width
  };
  
  return (
    <>
      <Box sx={{ flexGrow: 1, backgroundColor: "#303841", padding: "1em" }}>
        <Grid container spacing={2}>
          <Grid item xs={12} sm={5} sx={{ color: "white", marginTop: "10em", marginLeft: "2em" }}>
            <p style={fontSize1}>Hello, I am</p>
            <p style={fontSize2}>Kunathip</p>
            <p style={fontSize3}>
              Hello! I'm Por, a seasoned programmer with expertise in software and web application
              development. With experience in various businesses and projects, I am dedicated to
              creating efficient and user-centric applications.
            </p>

            <div
              style={{
                marginTop: "1em",
                display: "flex",
                flexDirection: "column",
                alignItems: "flex-start",
              }}
            >
              <h1 style={textWorkshop}>My workshop</h1>
              <div style={{ display: "flex", gap: "1em" , boder: '2px solid red'}}>
                <Link href="/workShop/kunathip/workshop-resume">
                  <Button variant="outlined" style={buttonNext}>
                    Resume
                  </Button>
                </Link>
                <Link href="/workShop/kunathip/workshop-calculator">
                  <Button variant="outlined" style={buttonNext}>
                    Workshop 1
                  </Button>
                </Link>
                <Link href="/workShop/kunathip/workshop-homco">
                  <Button variant="outlined" style={buttonNext}>
                    Workshop 2
                  </Button>
                </Link>
                <Link href="/workShop/kunathip/workshop-login/login">
                  <Button variant="outlined" style={buttonNext}>
                    Workshop 3/4
                  </Button>
                </Link>
              </div>
            </div>
          </Grid>
          <Grid item xs={12} sm={5} sx={{ marginTop: { xs: "2em", sm: "200px" }, alignItems: "right", marginLeft: "auto" }}>
            <img src="/imageknt/Profile.jpg" alt="Profile" style={imageProfile} />
          </Grid>
        </Grid>
      </Box>
    </>
  );
};

kunathip.getLayout = (kunathip) => <Layout>{kunathip}</Layout>;
export default kunathip;
