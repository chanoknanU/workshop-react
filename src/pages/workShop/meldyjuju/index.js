import { Layout} from "src/layouts/dashboard/layout";
import Navbar from "src/components/pang/navbar";
import Mycard from "src/components/pang/mycard";
import Jujustore from "src/components/pang/jujustore";
import Workshop1 from "src/components/pang/workshop1";

const meldyjuju = () => {
    return(
        <div>
        <Navbar />
        <Mycard />
        <Jujustore/>
        <Workshop1/>
        </div>

    )
}
meldyjuju.getLayout = (meldyjuju) => <Layout>{meldyjuju}</Layout>;
export default meldyjuju;