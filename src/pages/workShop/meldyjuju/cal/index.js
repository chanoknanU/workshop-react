import React from 'react'
import { Layout } from 'src/layouts/dashboard/layout';
import Navbar from 'src/components/pang/navbar';
import Cal from 'src/components/pang/cal';
import Typography from "@mui/material/Typography";

const cal = () => {
  return (
    <div>
        <Navbar />
        <Typography
        variant="h3"
        mt={5}
        mb={5}
        sx={{
            textAlign: "center"
        }}>WORKSHOP1</Typography>
        <Typography
            variant="h3"
            fontWeight="bold"
            mb={2}
            mt={1}
            sx={{
              textAlign: "center",
              letterSpacing: 5,
              border: 1,
              padding: "5px",
              backgroundColor: "#9292D1",
              color: "#fff",
              borderRadius: "8px",
            }}
          >
            Calculator by me
          </Typography>
        <Cal />
        </div>
    
  )
}
cal.getLayout = (page) => <Layout>{page}</Layout>;
export default cal