import React from "react";
import Navui from "src/components/miw/Navui";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import Footui from "src/components/miw/Footui";
import Bodyui1 from "src/components/miw/Bodyui1";
import Bodyui2 from "src/components/miw/Bodyui2";
import Bodyii0 from "src/components/miw/Bodyii0";

const ui2 = () => {
  return (
    <>
      <Navui />
      <div style={{ backgroundColor: "#f2eddd", padding: "100px" }}>
        <Typography
          variant="h1"
          sx={{
            color: "#455a64",
            margin: "10px",
            display: "block",
            textAlign: "left",
            fontWeight: "bold",
            fontSize: "80px",
          }}
          className="text-9xl "
        >
          SINGLE  POST
          <Typography variant="body1">
            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, lutus
            <br />
            nec ullamcorper mattis, pulvinar dapibus leo.
          </Typography>
        </Typography>
      </div>
      {/* <Bodyii0 /> */}
      <Box
        sx={{
          position: "relative",
          width: "100%",
          height: "200px",
          backgroundColor: "white",
        }}
      >
        <Box
          sx={{
            marginX: "5rem",
            marginBottom: "5rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,-4em)",
            zIndex: 2,
          }}
        >
          <Bodyii0 />
        </Box>
      </Box>

      <Box
        sx={{
          position: "relative",
          width: "100%",
          height: "100px",
          backgroundColor: "white",
        }}
      >
        <Box
          sx={{
            marginX: "5rem",
            marginBottom: "5rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,+32em)",
            zIndex: 2,
          }}
        >
          <Bodyui1 />
        </Box>

        <Box
          sx={{
            marginX: "12rem",
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,+30em)",
            zIndex: 30,
          }}
        >
          {" "}
          <Bodyui2 />
        </Box>
        <Box
          sx={{
            position: "absolute",
            top: "0",
            left: "0",
            bottom: "0",
            right: "0",
            transform: "translate(0,+46em)",
          }}
        >
          <Footui />
        </Box>
      </Box>
    </>
  );
};

export default ui2;
