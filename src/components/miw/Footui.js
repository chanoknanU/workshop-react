import React from "react";
import Container from "@mui/material/Container";
import { styled } from "@mui/system";
import { Box, Typography, TextField, Button } from "@mui/material";

const FooterContainer = styled("footer")({
  backgroundColor: "#f2eddd",
  padding: "70px 0",
  marginTop: "auto",
});

const Footui = () => {
  return (
    <FooterContainer>
      {/* <Container style={{ display: "flex", flexDirection: "row", justifyContent: "center" }}> */}
      <div style={{ width: "100%" }}>
        <Box sx={{ display: "grid", gridTemplateColumns: "repeat(3, 1fr)", marginX: "6rem",marginY:"7rem" }}>
          <Typography variant="h6">
            INFORMATION
            <Typography>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit.
              <br />
              adipiscing elit.Ut elit tellus, luctus
              <br />
              consectetur adipiscing elit.
            </Typography>
          </Typography>
          <Typography variant="h6">NAVIGATION</Typography>
          <Box>
            <Typography variant="h6">CONTACT US</Typography>
            <TextField label="Enter your email" variant="outlined" fullWidth margin="normal" color="primary"/>
            <Button variant="contained" color="primary" >
              Subscribe
            </Button>
          </Box>
        </Box>
      </div>
      {/* </Container> */}
    </FooterContainer>
  );
};

export default Footui;
