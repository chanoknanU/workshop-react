import { useState, useEffect } from "react";
import { Button, TextField, Container, Typography } from "@mui/material";

import { useRouter } from "next/navigation";
const Login = () => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const { push } = useRouter();
  const handleLogin = async () => {

    if (username === "" || password === ""){ // ถ้า username หรือ password มีค่าเท่ากับ "" หรือไม่ได้กรอกสักตัวนึง
      console.error("Error: กรุณากรอก username และ password") // แจ้ง error ใน console
      return // ออกจาก function handle login
    }



    try {
      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username,
          password,
        }),
      });
      if (!response.ok) {
        const errorData = await response.json(); // Assuming the server sends JSON error details
        console.log(errorData)
        throw new Error(`Login failed: ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง ${response.status} `); // ถ้าหลังบ้านตรวจสอบแล้ว username || password ผิดพลาด จะส่ง status และ ข้อความกลับมา
      }

      const data = await response.json();

      console.info("Info: Login successful");
      console.log("Response:", data);

      // Store JWT in local storage
      localStorage.setItem("jwt", data.accessToken);

      // Fetch user data using the obtained JWT

      push("/workShop/chanoknan/workshop4");
    } catch (error) {
      // Log more information about the error
      if (error instanceof Error) {
        console.error("Error Message:", error.message);
      }else{
        console.error("Error: Login failed", error);

      }
    }
  };

  return (
    <Container component="main" maxWidth="xs">
      <div>
        <Typography component="h1" variant="h5">
          Login
        </Typography>
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          id="username"
          label="Username"
          name="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <TextField
          variant="outlined"
          margin="normal"
          fullWidth
          name="password"
          label="Password"
          type="password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button fullWidth variant="contained" color="primary" onClick={handleLogin}>
          Login
        </Button>

        {/* Display user information if available */}
      </div>
    </Container>
  );
};

export default Login;
