import { useState, useEffect } from "react";
import { Button, Container, Typography, CircularProgress } from "@mui/material";

const Workshop5 = () => {
  const [userData, setUserData] = useState(null);
  const [loading, setLoading] = useState(false);
  console.log(  process.env.NEXT_PUBLIC_API_URL+"?search=karn")
  const handleApiCall = async (apiUrl,method,body) => {
    try {
      setLoading(true);
      console.log(body,method)
      const response = await fetch(apiUrl, {
        method: method,
        body:JSON.stringify(body),
        headers: {
          'Content-Type': 'application/json',
          // Authorization: `Bearer ${localStorage.getItem ("jwt")}`,
        },
      });

      if (!response.ok) {
        const errorData = await response.json();
        throw new Error(`${response.status} - ${errorData.message}`);
      }

      const data = await response.json();

      console.info("Info: API Call Successful");
      console.log("Response:", data);


      setUserData(data);
    } catch (error) {
      console.error("Error: API Call Failed", error);
      if (error instanceof Error) {
        console.error("Error Message:", error.message);
      }
    } finally {
      setLoading(false);
    }
  };

  const handleApiButtonClick = async () => {
    // Fetch users data
    await handleApiCall("https://www.melivecode.com/api/users","GET");
  };

  const handleApiButtonClickNew = async () => {
  
    // Fetch new set of users data
    await handleApiCall(process.env.NEXT_PUBLIC_API_URL+"/users?search=karn");
    
  };
  const handleApiButtonClickNewPOST = async () => {
    
    // Fetch new set of users data
    const body = {
      "fname": "w",
      "lname": "w",
      "username": "w.chat@melivecode.com",
      "password": "w",
      "email": "w.chat@melivecode.com",
      "avatar": "https://www.melivecode.com/users/cat.png"
    }
    await handleApiCall(process.env.NEXT_PUBLIC_API_URL+"/users/create","POST",body);
    
  };
  const handleApiButtonClickNewPOSTPOST = async () => {
    
    // Fetch new set of users data
    const body = {
      "id": 13,
      "lname": "Gato"
    }
    await handleApiCall(process.env.NEXT_PUBLIC_API_URL+"/users/update","PUT",body);
    
  };
  const handleApiButtonClickNewGETGET = async () => {
  
    // Fetch new set of users data
    await handleApiCall(process.env.NEXT_PUBLIC_API_URL+"/users?page=1&per_page=10","GET");
    
  };


  return (
    <Container component="main" maxWidth="xs">
      <div>
      <Typography variant="h6" sx={{ textAlign: 'center' }}>
          Workshop5:
        </Typography>
        <Button
          variant="contained"
          color="primary"
          onClick={handleApiButtonClick}
          sx={{ marginRight: 1 }} 
        >
          User List (1)
        </Button>
        <Button variant="contained" color="secondary" onClick={handleApiButtonClickNew}>
          User List (SEARCH 2)
        </Button>
        <Button variant="contained" color="secondary" onClick={handleApiButtonClickNewGETGET}>
          User List (3)
        </Button>
        <Button variant="contained" color="secondary" onClick={handleApiButtonClickNewPOST}>
          User Create (SEARCH 2)
        </Button>
        <Button variant="contained" color="secondary" onClick={handleApiButtonClickNewPOSTPOST}>
          User Create (Put)
        </Button>
        

        {loading && <CircularProgress />}
        {userData &&
          Array.isArray(userData) &&
          userData.map((user, index) => (
            <div key={index}>
              <Typography>Username: {user.username}</Typography>
              <Typography>Email: {user.email}</Typography>
              <Typography>First Name: {user.fname}</Typography>
              <Typography>Last Name: {user.lname}</Typography>

              {user.avatar && (
                <img
                  src={user.avatar}
                  alt={`User Avatar ${user.id}`}
                  style={{ width: "300px", height: "300px" }}
                />
              )}
            </div>
          ))}
      </div>
    </Container>
  );
};

export default Workshop5;
