import { Circle } from "@mui/icons-material";
import { Box, Button, CircularProgress, TextField, Typography } from "@mui/material";
import { useState } from "react";

const UserDetail = () => {
    const [userId, setUserId] = useState("");
    const [userData, setUserData] = useState(null);
    const [loading, setLoading] = useState(false);
    const [error, setError] = useState(null);

    const fetchUserData = async () => {
        setLoading(true);

        try {
            const response = await fetch(`https://www.melivecode.com/api/users/${userId}`);
            if (!response.ok) {
                const errorData = await response.json();
                throw new Error(`HTTP error! Status: ${response.status}, Message: ${errorData.message}`);
            }
            const result = await response.json();

            console.info('[INFO] Fetched user detail successfully')
            console.log('UserDetail', result);
            setUserData(result.user);
        } catch (error) {
            setError(error.message);
        } finally {
            setLoading(false);
        }
    };

    const handleUserIdChange = (event) => {
        setUserId(event.target.value);
    };

    const handleKeyPress = (e) => {
        if (e.key === "Enter") {
            fetchUserData();
        }
    };

    return (
        <>
            <TextField
                label="User ID"
                variant="outlined"
                type="number"
                value={userId}
                onChange={handleUserIdChange}
                onKeyDown={handleKeyPress}
                sx={{ my: 2 }}
            />
            <Button variant="contained" color="primary" onClick={fetchUserData}>
                Fetch User
            </Button>
            {loading && <CircularProgress style={{ marginTop: 20 }} />}
            {error && <Typography variant="body1" color="error">{error}</Typography>}
            {userData && (
                <Box sx={{ display: 'flex', gap: 4, mt: 4 }}>
                    <Box sx={{ position: 'relative' }}>
                        <img src={userData?.avatar} alt='Avatar' style={{
                            width: '120px', height: '120px',
                            borderRadius: '50%', outline: '1px solid #62626240',
                            outlineOffset: '5px'
                        }} />
                        <Circle sx={{ position: 'absolute', right: 0, bottom: 6, color: '#23c369', border: '1px solid #62626240', outline: '3px solid white', borderRadius: '50%', outlineOffset: -4, fontSize: '1.75em' }} />
                    </Box>
                    <Box>
                        <Typography variant="button">{`ID: ${userData?.id}`}</Typography>
                        <Typography variant="h6" mb={1}>Name: {userData?.fname} {userData?.lname}</Typography>
                        <Typography variant="body1">Username: {userData?.username}</Typography>
                        <Typography variant="body1">Email: {userData?.email}</Typography>
                    </Box>
                </Box>
            )}
        </>
    );
};

export default UserDetail;