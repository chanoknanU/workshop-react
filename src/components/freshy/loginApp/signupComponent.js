import Head from 'next/head';
import NextLink from 'next/link';
import { useState, useEffect } from 'react'; // Import useState and useEffect
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { Box, Button, Link, Stack, TextField, Typography, Input, CircularProgress } from '@mui/material';
import { CloudUploadRounded as CloudUploadIcon, UploadRounded as UploadRoundedIcon, BorderColorRounded as BorderColorRounededIcon } from '@mui/icons-material';
import defaultAvatar from 'public/assets/freshy/avatars/defaultAvatar.svg';

const baseUrl = '/workShop/teerawut/loginApp';
const SignupComponent = ({ setSnackbarMessage, setOpenSnackbar }) => {
    const [isRegister, setIsRegister] = useState(false);
    const [loading, setLoading] = useState(false);
    const [avatar, setAvatar] = useState(null);
    const [avatarPreview, setAvatarPreview] = useState(null); // Added state for avatar preview;
    const [fileSize, setFileSize] = useState(null);

    const handleAvatarChange = (event) => {
        const file = event.target.files[0];
        // Create a preview URL for the avatar
        if (file) {
            const sizeInKB = file.size / 1024;
            setFileSize(sizeInKB / 1024);
            setAvatarPreview(URL.createObjectURL(file));
            console.info('[INFO] User changed his/her avatar', `[${(sizeInKB / 1024).toFixed(2)} MB]`);

            // Set Avatar for pre-upload
            setAvatar(file);
            console.info('[INFO] New avatar is in a state');
        } else {
            console.error('[ERROR] Something happens on choose avatar image');
        }
    };

    const formik = useFormik({
        initialValues: {
            email: '',
            fname: '',
            lname: '',
            password: '',
            submit: null,
        },
        validationSchema: Yup.object({
            email: Yup.string().email('Must be a valid email').max(255).required('Email is required'),
            fname: Yup.string().max(255).required('First name is required'),
            lname: Yup.string().max(255).required('Last name is required'),
            password: Yup.string().max(255).required('Password is required'),
        }),
        onSubmit: async (values, helpers) => {
            try {
                setLoading(true);

                // Convert avatar file to Base64
                let avatarBase64 = '';
                if (avatar) {
                    const reader = new FileReader();
                    reader.onload = (e) => {
                        avatarBase64 = e.target.result;
                        console.info('[INFO] Converted user avatar to base64')
                        sendUserData(values, avatarBase64);
                    };
                    reader.readAsDataURL(avatar);
                } else {
                    sendUserData(values, avatarBase64);
                }
            } catch (err) {
                console.error('[ERROR] Error during user creation:', err);
                helpers.setErrors({ submit: 'An error occurred during user creation, or image is too large.' });
            } finally {
                setLoading(false);
            }
        },
    });

    const sendUserData = async (values, avatarBase64) => {
        try {
            setLoading(true);
            const response = await fetch(`${process.env.NEXT_PUBLIC_APP_HOST}/users/create`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    fname: values.fname,
                    lname: values.lname,
                    username: values.email,
                    password: values.password,
                    email: values.email,
                    avatar: avatarBase64,
                }),
            });

            const data = await response.json();

            if (response.ok) {
                console.info('[INFO] User created successfully');
                setSnackbarMessage({ status: 'success', title: 'Register successful' });
                handleReset();
            } else {
                console.error('[ERROR] User creation failed:', data.message);
                setSnackbarMessage({ status: 'failed', title: `User creation failed: ${data.message}` });
                formik.setErrors({ submit: data.message });
            }
            setLoading(false);
            setOpenSnackbar(true);
        } catch (err) {
            console.error('[ERROR] Error during user creation:', err);
            formik.setErrors({ submit: 'An error occurred during user creation.' });
        }
    };

    const handleReset = () => {
        formik.resetForm();
        setIsRegister(true);
        setAvatar(null);
        setAvatarPreview(null);
        setFileSize(null);
    }

    return (
        <>
            {isRegister ? (
                <>
                    <Typography variant="h5" mt={4}>Congrats!</Typography>
                    <Typography color="text.secondary" variant="body2">
                        You created an account successfully.
                    </Typography>
                    <Button
                        fullWidth
                        size="large"
                        sx={{ mt: 3 }}
                        variant="contained"
                        onClick={() => setIsRegister(false)}
                    >
                        Back to create again!
                    </Button>
                </>
            ) : (
                <form noValidate onSubmit={formik.handleSubmit}>
                    <Stack spacing={3} my={2}>
                        {/* Display the avatar preview */}
                        <Box sx={{ width: 'auto', minWidth: '20vw', display: 'flex', justifyContent: 'center', position: 'relative' }}>
                            <label htmlFor="avatar">
                                <Input
                                    id="avatar"
                                    name="avatar"
                                    type="file"
                                    accept="image/*"
                                    onChange={handleAvatarChange}
                                    style={{ display: 'none' }}
                                />
                                <img src={avatarPreview ? avatarPreview : defaultAvatar.src}
                                    style={{
                                        borderRadius: '50%',
                                        boxShadow: '0 1px 10px 2px rgba(0, 0, 0, 0.1)',
                                        width: 160, height: 160, objectFit: 'cover'
                                    }}
                                    alt="Avatar Preview" />
                                <Box sx={{
                                    bgcolor: '#6366f1',
                                    position: 'absolute',
                                    bottom: 0, right: '28%',
                                    borderRadius: 2, p: 1,
                                    boxShadow: '0 1px 10px 2px rgba(0, 0, 0, 0.1)',
                                    cursor: 'pointer',
                                    transition: 'ease-out .25s',
                                    color: 'white',
                                    ":hover": {
                                        bgcolor: '#4338ca',
                                        transform: 'translateY(-0.25em)'
                                    }
                                }}>
                                    {avatarPreview ? <BorderColorRounededIcon /> : <UploadRoundedIcon />}
                                </Box>
                            </label>
                        </Box>
                        {fileSize &&
                            <Typography variant='caption' sx={{ textAlign: 'center', m: 0 }}>
                                Size: {fileSize?.toFixed(2)} MB
                            </Typography>
                        }
                        <TextField
                            error={!!(formik.touched.fname && formik.errors.fname)}
                            fullWidth
                            helperText={formik.touched.fname && formik.errors.fname}
                            label="First Name"
                            name="fname"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.fname}
                        />
                        <TextField
                            error={!!(formik.touched.lname && formik.errors.lname)}
                            fullWidth
                            helperText={formik.touched.lname && formik.errors.lname}
                            label="Last Name"
                            name="lname"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            value={formik.values.lname}
                        />
                        <TextField
                            error={!!(formik.touched.email && formik.errors.email)}
                            fullWidth
                            helperText={formik.touched.email && formik.errors.email}
                            label="Email Address"
                            name="email"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            type="email"
                            value={formik.values.email}
                        />
                        <TextField
                            error={!!(formik.touched.password && formik.errors.password)}
                            fullWidth
                            helperText={formik.touched.password && formik.errors.password}
                            label="Password"
                            name="password"
                            onBlur={formik.handleBlur}
                            onChange={formik.handleChange}
                            type="password"
                            value={formik.values.password}
                        />
                    </Stack>
                    {formik.errors.submit && (
                        <Typography color="error" sx={{ mt: 3 }} variant="body2">
                            {formik.errors.submit}
                        </Typography>
                    )}
                    <Button
                        fullWidth
                        size="large"
                        sx={{ mt: 3 }}
                        type="submit"
                        variant="contained"
                        disabled={loading}
                    >
                        {loading ? <CircularProgress size={24} /> : 'Continue'}
                    </Button>
                </form>
            )}
        </>
    );
};

export default SignupComponent;