import { Button, Link, Paper, Typography } from "@mui/material";
import { Box } from "@mui/system";

export default function Campaign() {
    return (
        <Paper
            elevation={0}
            sx={{
                display: 'flex',
                flexDirection: { xs: 'column', md: 'row' },
                justifyContent: 'center',
                alignItems: { xs: 'center', md: 'baseline' },
                fontFamily: 'Inter, sans-serif',
                mb: 8
            }}>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 2,
                    bgcolor: '#4BA4F2',
                    p: 4,
                    width: { xs: '100%', md: '22em' },
                    maxHeight: '22em',
                    boxShadow: '10px 10px #06223B'
                }}>
                <Typography
                    color='white'
                    variant="h5"
                    fontWeight="semibold">
                    HOME DESIGN CONSULTATION
                </Typography>
                <Typography
                    variant="body1"
                    color='white'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                </Typography>
                <Button
                    variant="text"
                    sx={{
                        textDecoration: 'none',
                        fontWeight: 'bold',
                        color: 'white',
                        display: 'block',
                        textAlign: 'left',
                        px: 0
                    }}>
                    {'-->'}
                </Button>
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 2,
                    bgcolor: '#4BB2F2',
                    p: 4, width: { xs: '100%', md: '22em' },
                    maxHeight: '22em',
                    boxShadow: '10px 10px #06223B'
                }}>
                <Typography
                    color='white'
                    variant="h5"
                    fontWeight="semibold">
                    HOME DESIGN 3D 2D INTERIOR SERVICE
                </Typography>
                <Typography
                    variant="body1"
                    color='white'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                </Typography>
                <Button
                    variant="text"
                    sx={{
                        textDecoration: 'none',
                        fontWeight: 'bold',
                        color: 'white',
                        display: 'block',
                        textAlign: 'left',
                        px: 0
                    }}>
                    {'-->'}
                </Button>
            </Box>
            <Box
                sx={{
                    display: 'flex',
                    flexDirection: 'column',
                    gap: 2,
                    bgcolor: '#3e88ec',
                    p: 4,
                    width: { xs: '100%', md: '22em' },
                    maxHeight: '22em',
                    boxShadow: '10px 10px #06223B'
                }}>
                <Typography
                    color='white'
                    variant="h5" f
                    ontWeight="semibold">
                    HOME DESIGN CONSULTATION
                </Typography>
                <Typography
                    variant="body1"
                    color='white'>
                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                </Typography>
                <Button
                    variant="text"
                    sx={{
                        textDecoration: 'none',
                        fontWeight: 'bold',
                        color: 'white',
                        display: 'block',
                        textAlign: 'left',
                        px: 0
                    }}>
                    {'-->'}
                </Button>
            </Box>
        </Paper>
    );
}