import { Container, Paper, Grid, Typography, List, TextField, Button } from "@mui/material";
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import ChevronRightRoundedIcon from '@mui/icons-material/ChevronRightRounded';
import CheckListItem from "./intro/checklist";
import LinkList from "./LinkList";
import { EmailRounded, LocationOnRounded } from "@mui/icons-material";
import Contact from "./contact";
const baseUrl = '/workShop/teerawut/designUI';

export default function FooterSection() {
    return (
        <Paper elevation={0} square sx={{ bgcolor: '#3e99ec', py: 8, pt: 38 }}>
            <Container maxWidth='xl' sx={{position: 'relative'}}>
                <Contact/>
                <Grid
                    container
                    spacing={4}
                    direction="row"
                    justifyContent="center"
                    alignItems="flex-start"
                    alignContent="stretch"
                    wrap="wrap"
                >
                    <Grid item xs={12} md={6}>
                        <Typography variant="h5" color='white'>
                            INFORMATION
                        </Typography>
                        <Typography variant="body1" color="white" sx={{ mt: 2, mb: 4 }}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                        </Typography>
                        <FacebookIcon sx={{ mr: 2, color: 'white', fontSize: '2em' }} />
                        <TwitterIcon sx={{ mr: 2, color: 'white', fontSize: '2em' }} />
                        <InstagramIcon sx={{ mr: 2, color: 'white', fontSize: '2em' }} />
                        <LinkedInIcon sx={{ mr: 2, color: 'white', fontSize: '2em' }} />
                    </Grid>
                    <Grid item xs={6} md={3} color='white'>
                        <Typography variant="h5" color='white'>
                            NAVIGATION
                        </Typography>
                        <List>
                            <LinkList
                                startIcon={<ChevronRightRoundedIcon />}
                                primary='Homepage'
                                iconColor='white'
                                textColor='white'
                                href={`${baseUrl}/`}
                            />
                            <LinkList
                                startIcon={<ChevronRightRoundedIcon />}
                                primary='About us'
                                iconColor='white'
                                textColor='white'
                                href={`${baseUrl}/about`}
                            />
                            <LinkList
                                startIcon={<ChevronRightRoundedIcon />}
                                primary='Services'
                                iconColor='white'
                                textColor='white'
                                href={`${baseUrl}/services`}
                            />
                            <LinkList
                                startIcon={<ChevronRightRoundedIcon />}
                                primary='Project'
                                iconColor='white'
                                textColor='white'
                                href={`${baseUrl}/project`}
                            />
                        </List>
                    </Grid>
                    <Grid item xs={6} md={3}>
                        <Typography variant="h5" color='white'>
                            CONTACT US
                        </Typography>
                        <List>
                             <CheckListItem startIcon={<LocationOnRounded/>} primary='Lumbung Hidup East Java' textColor='white' iconColor='white'/>
                             <CheckListItem startIcon={<EmailRounded/>} primary='Hello@Homco.com' textColor='white' iconColor='white'/>
                        </List>
                        <TextField id="filled-basic" label="Email Address" variant="filled"/>
                        <Button variant="contained" color="inherit" sx={{my: 1, py: 2, bgcolor: 'white'}}>
                          Subscribe
                        </Button>
                    </Grid>
                </Grid>
            </Container>
        </Paper>
    );
}
