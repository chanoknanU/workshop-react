import { Link, ListItem, ListItemIcon, ListItemText } from "@mui/material";

const LinkList = ({ startIcon, primary, iconColor, textColor, href }) => (
  <ListItem disableGutters>
    <Link href={href} style={{ display: 'flex', alignItems: 'center', color: textColor, textDecoration: 'none'}}>
      <ListItemIcon
        sx={{ minWidth: "fit-content", pr: 1, color: iconColor }}
      >
        {startIcon}
      </ListItemIcon>
      <ListItemText primary={primary} />
    </Link>
  </ListItem>
);

export default LinkList;