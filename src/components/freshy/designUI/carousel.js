import React from "react";
import { Paper, Slide, Box } from "@mui/material";
import Image from "next/image";

const slides = [
    {
        title: "Slide 1",
        image: "https://img.freepik.com/free-photo/stylish-scandinavian-living-room-with-design-mint-sofa-furnitures-mock-up-poster-map-plants-eleg_1258-152155.jpg?w=1380&t=st=1700541245~exp=1700541845~hmac=4321d3a5661939c4393403c1ab4cc42ebe6efae79a270a8e7e0cdd83e34e417a",
    },
    {
        title: "Slide 2",
        image: "https://img.freepik.com/free-photo/3d-rendering-minimalist-interior-with-copy-space_23-2150943528.jpg?w=1380&t=st=1700542200~exp=1700542800~hmac=a3843973c4a8e49b106abfedc6df1df65c6883e66f1a578a259f3b48d1e9bebf",
    },
    {
        title: "Slide 3",
        image: "https://img.freepik.com/free-photo/3d-rendering-minimalist-interior-with-copy-space_23-2150943531.jpg?w=1060&t=st=1700542211~exp=1700542811~hmac=71bf3331040457909260911d731a9efaa0042abb69d572e3cfaca3e512758b79",
    },
    {
        title: "Slide 4",
        image: "https://img.freepik.com/free-photo/modern-living-room-interior-design_23-2150794680.jpg?t=st=1700542221~exp=1700545821~hmac=d9aacaaf698468c250ae7941276b8019fd602aefea0cf26a89b1656ad4b1032b&w=1060",
    },
    {
        title: "Slide 5",
        image: "https://img.freepik.com/free-photo/room-decor-with-potted-plants-sofa_23-2149428021.jpg?w=1060&t=st=1700542237~exp=1700542837~hmac=53a7ca2b112b10b9b0bd187a6611746b697dfcf4f7cd5d6376952ad158d15c16",
    },
    {
        title: "Slide 6",
        image: "https://img.freepik.com/free-photo/luxury-domestic-kitchen-with-elegant-wooden-design-generated-by-ai_188544-15357.jpg?t=st=1700540900~exp=1700544500~hmac=741880dc23f4c00c3a1f6ac3b98e9f3bc086149ca7e008614ce01d322051c88b&w=1380",
    },
    {
        title: "Slide 7",
        image: "https://img.freepik.com/free-photo/modern-apartment-with-comfortable-sofa-decor-generated-by-ai_188544-38495.jpg?w=1380&t=st=1700542284~exp=1700542884~hmac=40e55b60c8562566e71aa4074f2244b7d2e2c971e0682885abbf34091e15bdaf",
    },
];

const Carousel = () => {
    const infiniteSlides = [...slides, ...slides, ...slides]; // Duplicate the slides to create an infinite loop

    return (
        <Box
            sx={{
                display: "flex",
                overflowX: "auto",
                position: 'relative',
                scrollSnapType: "x mandatory",
                whiteSpace: "nowrap",
                "&::-webkit-scrollbar": {
                    display: "none",
                },
                transform: 'skew(4deg, 3deg) translateY(-.5em)',
                gap: 2
            }}
        >
            {infiniteSlides.map((slide) => (
                <Slide key={slide.title} direction="left" in={true} mountOnEnter unmountOnExit>
                    <Paper
                        square
                        sx={{
                            textAlign: "center",
                            minWidth: "360px",
                            minHeight: "300px",
                            scrollSnapAlign: "start",
                        }}
                    >
                        <img src={slide.image} style={{width: 'auto', height: '100%', objectFit: 'cover'}} alt={slide.title}/>
                    </Paper>
                </Slide>
            ))}
        </Box>
    );
};

export default Carousel;
