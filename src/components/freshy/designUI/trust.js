import { Box, Grid, List, ListItem, ListItemIcon, ListItemText, Paper, Typography } from "@mui/material";
import Image from "next/image";
import InteriorPhoto4 from 'public/assets/freshy/designUI/interior4.jpg'
import { cloneElement, useState } from "react";
import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import CheckListItem from "./intro/checklist";
import InsightItem from "./intro/insight";
import TrustVideo from "./trust/video";

const trustObject = {
    intro: "Trust Us Now",
    title: "WHY CHOICE OUR HOME DESIGN INTERIOR SERVICES",
    subtitle: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.`,
    checkList: [
        { title: "Flexible Time", startIcon: <CheckCircleIcon /> },
        { title: "Perfect Work", startIcon: <CheckCircleIcon /> },
        { title: "Client Priority", startIcon: <CheckCircleIcon /> },
        { title: "Flexible Time", startIcon: <CheckCircleIcon /> },
        { title: "Perfect Work", startIcon: <CheckCircleIcon /> },
        { title: "Client Priority", startIcon: <CheckCircleIcon /> },
    ],
};


const TrustSection = () => {
    return (
        <Paper
            elevation={0}
            sx={{
                display: "flex",
                justifyContent: "center",
                fontFamily: "Inter, sans-serif",
                flexDirection: { xs: "column", md: "row" },
                my: { xs: 12, md: 24 },
                gap: 4,
            }}
        >
            <Box flex={1} sx={{position: 'relative', borderRadius: 1.5, overflow: 'hidden'}}>
                <TrustVideo/>
            </Box>
            <Box flex={1}>
                <Typography variant="overline" color="initial" sx={{ my: 2 }}>
                    {trustObject.intro}
                </Typography>
                <Typography
                    variant="h1"
                    color="initial"
                    sx={{
                        my: 2,
                        fontSize: { xs: '2em', sm: '3em', md: '3.5em' },
                    }}
                >
                    {trustObject.title}
                </Typography>
                <Typography
                    variant="subtitle1"
                    color="initial"
                    fontWeight="normal"
                    sx={{ my: 2 }}
                >
                    {trustObject.subtitle}
                </Typography>
                <Grid container spacing={2}>
                    <Grid item xs={6}>
                        <List>
                            {trustObject.checkList.slice(0, 3).map((item, index) => (
                                <CheckListItem
                                    key={index}
                                    startIcon={item.startIcon}
                                    primary={item.title}
                                    iconColor='#3e99ec'
                                />
                            ))}
                        </List>
                    </Grid>
                    <Grid item xs={6}>
                        <List>
                            {trustObject.checkList.slice(3).map((item, index) => (
                                <CheckListItem
                                    key={index}
                                    startIcon={item.startIcon}
                                    primary={item.title}
                                    iconColor='#3e99ec'
                                />
                            ))}
                        </List>
                    </Grid>
                </Grid>
            </Box>
        </Paper>
    );
};

export default TrustSection;