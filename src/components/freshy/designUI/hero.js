import React from 'react';
import { Paper, Typography, Button, Box } from '@mui/material';
import Image from 'next/image';
import InteriorPhoto from 'public/assets/freshy/designUI/interior.jpg'

const HeroSection = () => {
    return (
        <Box elevation={0}
            sx={{
                display: 'flex',
                flexDirection: { xs: 'column-reverse', md: 'row' },
                justifyContent: { xs: 'center', md: 'space-between' },
                alignItems: 'center',
                fontFamily: 'Inter, sans-serif',
                mb: {xs: 6, md: 0}
            }}>
            <Box
                flex={1}
                sx={{
                    textAlign: { xs: 'center', md: 'left' }
                }}>
                <Typography
                    variant="overline"
                >
                    WELCOME TO HOMCO
                </Typography>
                <Typography
                    variant="h1"
                    sx={{
                        fontSize: { xs: '2em', sm: '3em', md: '4em', lg: '5em' },
                        my: 2
                    }}>
                    BUILD YOUR ELEGAN DREAM HOME INTERIOR
                </Typography>
                <Typography
                    variant="subtitle1"
                    sx={{ my: 2 }}>
                    Crafting timeless elegance for your dream home interiors—where sophistication meets comfort, and every detail tells a story.
                </Typography>
                <Button
                    variant="contained"
                    color="inherit"
                    sx={{
                        my: 3,
                        bgcolor: '#3e99ec',
                        color: 'white',
                        py: 2,
                        px: 4,
                        "&:hover": {
                            bgcolor: '#3077b9'
                        }
                    }}>
                    Get Started
                </Button>
            </Box>
            <Box flex={1}>
                <Image src={InteriorPhoto} alt='Interior' />
            </Box>
        </Box>
    );
};

export default HeroSection;
