import React, { useState, useEffect, Suspense } from 'react';
import Autocomplete from '@mui/material/Autocomplete';
import TextField from '@mui/material/TextField';

const apiKey = 'df43a3f1-23cd-485e-a31a-2ef59b5eced5';

const CityAutoComplete = ({ country, state, handleCity, selectedCity }) => {
    const [cities, setCities] = useState([]);
    // const myCities = cities.map((city) => city.city) || [];

    useEffect(() => {
        const fetchCities = async () => {
            const apiUrl = `http://api.airvisual.com/v2/cities?state=${state}&country=${country}&key=${apiKey}`;
            try {
                const response = await fetch(apiUrl);
                const result = await response.json();
                console.log(result);
                setCities(result.data?.map((city) => city.city) || []);
            } catch (error) {
                console.error('Error fetching cities:', error);
            }
        };

        if (country && state) {
            fetchCities();
            console.log(cities)
        }
    }, [country, state]);

    return (
        <Suspense fallback={<p>Loading City...</p>}>
            <Autocomplete
                disablePortal
                sx={{ my: 2 }}
                options={cities}
                value={selectedCity}
                onChange={(event, newValue) => {
                    handleCity(newValue);
                }}
                renderInput={(params) => (
                    <TextField {...params} label="Select City" variant="outlined" />
                )}
            />
        </Suspense>
    );
};

export default CityAutoComplete;
