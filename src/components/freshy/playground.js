import React, { useState } from 'react';
import { Tabs, Tab, Paper, Typography, Button, Tooltip, TextField } from '@mui/material';
import { Box } from '@mui/system';
import { TabPanel, ReposPanel } from './workshop';

export default function Playground() {
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    // Function format date to YYYY-MM-DD
    const formatDate = (dateString) => {
        const options = { year: 'numeric', month: 'long', day: 'numeric' };
        const formattedDate = new Date(dateString).toLocaleDateString(undefined, options);
        return formattedDate;
    };

    // Array of workshop data with title and subtitle
    const playgrounds = [
        {
            tabName: "Playground 1",
            title: 'Weather Web Application',
            subtitle: 'Web Application check weather, temperature and air quality.',
            date: '2023-11-17',
            reposLink: 'https://gitlab.com/trwfs00/workshop-react',
            reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
            thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-17%20144041.png?alt=media&token=dfb4352b-bd14-4b47-aa29-39aee55a04d9',
            demoHref: '/weatherApp'
        },
        {
            tabName: "Playground 2",
            title: 'Melivecode JWT Authentication',
            subtitle: 'Playing with Melivecode Authentication API',
            date: '2023-11-22',
            reposLink: 'https://gitlab.com/trwfs00/workshop-react',
            reposHTTPS: 'https://gitlab.com/trwfs00/workshop-react.git',
            thumbnail: 'https://firebasestorage.googleapis.com/v0/b/mobileweb-1f970.appspot.com/o/Screenshot%202023-11-17%20144041.png?alt=media&token=dfb4352b-bd14-4b47-aa29-39aee55a04d9',
            demoHref: '/loginApp/playground'
        },
    ];

    return (
        <Paper className='px-8 py-4'>
            <Tabs
                value={value}
                onChange={handleChange}
                variant="scrollable"
                scrollButtons="auto"
                allowScrollButtonsMobile
                textColor="inherit"
                indicatorColor="none"
                aria-label="Workshop List"
                className='mb-8 text-green-600'
            >
                {/* Generate Tab components using map */}
                {playgrounds.map((workshop, index) => (
                    <Tab key={index} label={workshop.tabName} />
                ))}
            </Tabs>

            {/* Generate TabPanel components using map */}
            {playgrounds.map((workshop, index) => (
                <TabPanel key={index} value={value} index={index}>
                    <Box className="flex flex-col md:flex-row justify-between mb-6">
                        <Box>
                            <Typography variant="h6" sx={{ mb: 1 }}>{workshop.title}</Typography>
                            <Typography variant="subtitle1">{workshop.subtitle}</Typography>
                        </Box>
                            <Typography variant="body2">Date: {formatDate(workshop.date)}</Typography>
                    </Box>
                    <img src={workshop.thumbnail} className='h-1/2 md:h-[36em] w-full object-cover mb-8 rounded-md ring-1 ring-offset-4 ring-gray-200' alt={workshop.title} />
                    <ReposPanel reposLink={workshop.reposLink} reposHTTPS={workshop.reposHTTPS} demoHref={workshop.demoHref} playgrounds/>
                </TabPanel>
            ))}
        </Paper>
    );
}
