import Navbar from "./navbar";
import * as React from 'react';
import Image from "next/image";
import Box from '@mui/material/Box';
import Card from '@mui/material/Card';
import CardContent from '@mui/material/CardContent';
import { Grid,CardActions, Button,Typography, Avatar } from "@mui/material";
import Workshop3 from "src/pages/workShop/onrain/workshop3";





async function User() {
    const accessToken = localStorage.getItem('accessToken');

    return fetch(`${process.env.NEXT_PUBLIC_AUTH_USER}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${accessToken}`
            
        }
    })
        .then(data => data.json())
}

const Workshop4Compo = () => {
    const [userData, setUserData] = React.useState(null);

    React.useEffect(() => {

        User().then(data => {
            localStorage.setItem('user', JSON.stringify(data.user));
            const userstored = localStorage.getItem('user')
            const userinfo = JSON.parse(userstored)
            console.log("info: " + userstored)
            setUserData(userinfo);
        })

            .catch(error => {
                console.error('Error fetching user data:', error);
            });
    }, []);

    const user = localStorage.getItem('user');
    const accessToken = localStorage.getItem('accessToken');

    const handleLogout = () => {
        localStorage.removeItem("accessToken");
        localStorage.removeItem("user");
        window.location.href = "/workShop/onrain/workshop3";
    };

    if (user === 'undefined' || !user || !accessToken) {
       return <Workshop3/>
    } else {
        return (
            <>
                <Navbar />
                <Box sx={{ flexGrow: 1, mt: 5 }}>
                    <Grid container
                        spacing={3}>
                        <Grid item
                            xs>
                        </Grid>
                        <Grid item
                            xs={6}>
                            <Card sx={{ minWidth: 275 }}>
                                <CardContent>
                                    {userData && (
                                        <Box sx={{ display: 'flex' }}>
                                            <Avatar src={userData.avatar}
                                               sx={{width:'100px',height:'100px'}}
                                                alt="user-img" />
                                            <Box ml={3}>
                                            <Typography variant="h6" component="div">
                                                name: {userData.fname} {userData.lname}
                                            </Typography>
                                            <Typography component="p">
                                                username: {userData.username}
                                            </Typography>
                                              
                                            </Box>
                                        </Box>
                                    )}
                                </CardContent>
                                <CardActions>
                                    <Button fullWidth onClick={handleLogout} sx={{color:"red"}}>Logout</Button>
                                </CardActions>
                            </Card>
                        </Grid>
                        <Grid item
                            xs>
                        </Grid>
                    </Grid>
                </Box>


            </>
        )
    }


}
export default Workshop4Compo;