

import Image from 'next/image';

import Navbar from './navbar';
import styles from 'src/styles/profile.module.css';
import Box from '@mui/material/Box';
import LocalPhoneRoundedIcon from '@mui/icons-material/LocalPhoneRounded';
import AlternateEmailRoundedIcon from '@mui/icons-material/AlternateEmailRounded';
import HomeRoundedIcon from '@mui/icons-material/HomeRounded';

import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { Pagination } from '@mui/material';



import React, { useState } from 'react';
import Head from 'next/head';


function createData(number, title, credits) {
    return { number, title, credits };
}

const rows = [
    createData('342117', 'STRUCTURED PROGRAMMING FOR INFORMATION TECHNOLOGY', 3),
    createData('342181', 'LOGIC AND CONCEPTS', 3),
    createData('342182', 'INSPIRATION IN IT CAREER', 1),
    createData('GE141166', 'SCIENCE OF HAPPINESS', 3),
    createData('GE151144', 'MULTICULTURALISM', 3),
    createData('GE321415', 'LEARNING SKILLS', 3),
    createData('LI101001', 'ENGLISH I ', 3),
];

export default function Orn() {
    const [currentPage, setCurrentPage] = useState(1);
    const dataPerPage = 5;
    const start = (currentPage - 1) * dataPerPage;
    const end = start + dataPerPage;
    const paginatedData = rows.slice(start, end);

    const handlePagination = (event,page) => {
        setCurrentPage(page);

    };


    return (
        <>
        <Head>
            <title>หน้าแรก | อร</title>
            <meta name='keywords' 
                content='My Home'/>
        </Head>
            <Navbar />
            <div className="profileBg">
                <Box sx={{ display: 'flex' }}>
                    <Box>
                        <Image className={styles.profileImage}
                            src="/profile.jpg"
                            width={200}
                            height={200} />
                        <h2 className={styles.profileInfo}>Ornin  Phongphaew</h2>
                        <div className={`${styles["profileInfo"]} ${styles["aboutMe"]}`}>
                            <p><AlternateEmailRoundedIcon className={styles.icon} /> <span>orninpp@gmail.com</span></p>
                            <p><LocalPhoneRoundedIcon className={styles.icon} /> <span>0829986238</span></p>
                            <p><HomeRoundedIcon className={styles.icon} /> <span>Khonkaen, Thailand</span></p>
                        </div>
                    </Box>
                    <Box m={2}>
                        <h2>About me</h2>
                        <p className={styles.aboutMe}>I am  currently pursuing a Bachelors degree in Information Technology slated for completion in 2024. Driven by a profound passion for technology, I have embraced an immersive journey exploring the intricacies of IT. From coding languages to system architecture, my academic pursuits have honed my skills, while my penchant for innovation fuels my desire to make impactful contributions in the evolving tech landscape.</p>
                        <h2>Course work</h2>
                        <TableContainer component={Paper}>
                            <Table sx={{ minWidth: 650 }}
                                aria-label="simple table">
                                <TableHead>
                                    <TableRow>
                                        <TableCell>COURSE NO</TableCell>
                                        <TableCell>COURSE TITLE</TableCell>
                                        <TableCell align='center'>CREDITS</TableCell>
                                    </TableRow>
                                </TableHead>
                                <TableBody>
                                    {paginatedData.map((row) => (
                                        <TableRow
                                            key={row.number}
                                            sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                        >
                                            <TableCell>{row.number}</TableCell>
                                            <TableCell component="th"
                                                scope="row">
                                                {row.title}
                                            </TableCell>
                                            <TableCell align='center'>{row.credits}</TableCell>

                                        </TableRow>
                                    ))}
                                </TableBody>
                            </Table>
                        </TableContainer>
                        <div className={styles.pagination}>
                            <Pagination
                                count={Math.ceil(rows.length / dataPerPage)}
                                page={currentPage}
                                onChange={handlePagination}
                                variant="outlined"
                                shape="rounded"
                                
                            />
                        </div>

                    </Box>
                </Box>
            </div>


        </>

    )
}
