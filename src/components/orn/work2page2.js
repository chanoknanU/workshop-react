import FooterWork2 from "./footerWork2";
import Navbar from "./navbar";
import NavbarWork2 from "./navbarWork2";
import { Box } from "@mui/material";
import * as React from 'react';
import List from '@mui/material/List';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import Collapse from '@mui/material/Collapse';
import ExpandLess from '@mui/icons-material/ExpandLess';
import ExpandMore from '@mui/icons-material/ExpandMore';


const Work2page2 = () => {
    const [open, setOpen] = React.useState(false);

    const handleClick = () => {
        setOpen(!open);
    };
    const [open2, setOpen2] = React.useState(false);

    const handleClick2 = () => {
        setOpen2(!open2);
    };
    const [open3, setOpen3] = React.useState(false);

    const handleClick3 = () => {
        setOpen3(!open3);
    };
    const [open4, setOpen4] = React.useState(false);

    const handleClick4 = () => {
        setOpen4(!open4);
    };
    const [open5, setOpen5] = React.useState(false);

    const handleClick5 = () => {
        setOpen5(!open5);
    };
    const [open6, setOpen6] = React.useState(false);

    const handleClick6 = () => {
        setOpen6(!open6);
    };
    return (
        <>
            <Navbar />
            <NavbarWork2 />
            <Box sx={{ backgroundColor: '#4F6F52', p: 3, pt: 10, pb: 10 }}>
                <Box sx={{ color: '#FFF', fontWeight: '900', fontSize: '50px' }}>FAQ</Box>
                <Box sx={{ color: '#FFF', mt: 1 }}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</Box>
            </Box>
            <Box sx={{ display: 'flex', mt: 6 }}>
                <Box sx={{ width: '50%', m: 3, backgroundColor: '#739072', height: '50vh' }}></Box>
                <Box sx={{ width: '50%', m: 3 }}>
                    <Box sx={{ fontWeight: 'bold', color: '#4F6F52' }}>FIRST QUESTION ANSWER</Box>
                    <div style={{ fontSize: '30px', fontWeight: '900', marginTop: '1rem' }} >QUESTION ANSWER TRENDING WEEKLY</div>
                    <Box mt={1}
                        sx={{ color: '#6E6E6E' }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                    </Box>
                    <List
                        sx={{ width: '100%', bgcolor: 'background.paper', borderBottom: '1px solid #DADADA' }}
                        component="nav"
                        aria-labelledby="nested-list-subheader"

                    >
                        <ListItemButton onClick={handleClick}>
                            <ListItemText>
                                <Box sx={{ fontWeight: 'bold' }}>How Long Day Needed ?</Box>
                            </ListItemText>
                            {open ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={open}
                            timeout="auto"
                            unmountOnExit>
                            <List component="div"
                                disablePadding>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>

                            </List>
                        </Collapse>
                    </List>
                    <List
                        sx={{ width: '100%', bgcolor: 'background.paper', borderBottom: '1px solid #DADADA' }}
                        component="nav"
                        aria-labelledby="nested-list-subheader"

                    >
                        <ListItemButton onClick={handleClick2}>
                            <ListItemText>
                                <Box sx={{ fontWeight: 'bold' }}>How To Claim Insurance ?</Box>
                            </ListItemText>
                            {open2 ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={open2}
                            timeout="auto"
                            unmountOnExit>
                            <List component="div"
                                disablePadding>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>

                            </List>
                        </Collapse>
                    </List>
                    <List
                        sx={{ width: '100%', bgcolor: 'background.paper', borderBottom: '1px solid #DADADA' }}
                        component="nav"
                        aria-labelledby="nested-list-subheader"

                    >
                        <ListItemButton onClick={handleClick3}>
                            <ListItemText>
                                <Box sx={{ fontWeight: 'bold' }}>Can I Request People Working ?</Box>
                            </ListItemText>
                            {open3 ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={open3}
                            timeout="auto"
                            unmountOnExit>
                            <List component="div"
                                disablePadding>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>

                            </List>
                        </Collapse>
                    </List>
                </Box>
            </Box>
            <Box sx={{ display: 'flex', mt: 6 }}>
                <Box sx={{ width: '50%', m: 3 }}>
                    <Box sx={{ fontWeight: 'bold', color: '#4F6F52' }}>SECOND QUESTION ANSWER</Box>
                    <div style={{ fontSize: '30px', fontWeight: '900', marginTop: '1rem' }} >USUALLY ASKED QUESTION CLIENTS</div>
                    <Box mt={1}
                        sx={{ color: '#6E6E6E' }}>
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                    </Box>
                    <List
                        sx={{ width: '100%', bgcolor: 'background.paper', borderBottom: '1px solid #DADADA' }}
                        component="nav"
                        aria-labelledby="nested-list-subheader"

                    >
                        <ListItemButton onClick={handleClick4}>
                            <ListItemText>
                                <Box sx={{ fontWeight: 'bold' }}>Where I Get Meeting Day ?</Box>
                            </ListItemText>
                            {open4 ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={open4}
                            timeout="auto"
                            unmountOnExit>
                            <List component="div"
                                disablePadding>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>

                            </List>
                        </Collapse>
                    </List>
                    <List
                        sx={{ width: '100%', bgcolor: 'background.paper', borderBottom: '1px solid #DADADA' }}
                        component="nav"
                        aria-labelledby="nested-list-subheader"

                    >
                        <ListItemButton onClick={handleClick5}>
                            <ListItemText>
                                <Box sx={{ fontWeight: 'bold' }}>Can Homco Pick Me To Office ?</Box>
                            </ListItemText>
                            {open5 ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={open5}
                            timeout="auto"
                            unmountOnExit>
                            <List component="div"
                                disablePadding>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>

                            </List>
                        </Collapse>
                    </List>
                    <List
                        sx={{ width: '100%', bgcolor: 'background.paper', borderBottom: '1px solid #DADADA' }}
                        component="nav"
                        aria-labelledby="nested-list-subheader"

                    >
                        <ListItemButton onClick={handleClick6}>
                            <ListItemText>
                                <Box sx={{ fontWeight: 'bold' }}>Can I Credit This Price ?</Box>
                            </ListItemText>
                            {open6 ? <ExpandLess /> : <ExpandMore />}
                        </ListItemButton>
                        <Collapse in={open6}
                            timeout="auto"
                            unmountOnExit>
                            <List component="div"
                                disablePadding>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>
                                <ListItemButton sx={{ pl: 4 }}>
                                    <ListItemText primary="Starred" />
                                </ListItemButton>

                            </List>
                        </Collapse>
                    </List>
                </Box>
                <Box sx={{ width: '50%', m: 3, backgroundColor: '#739072', height: '50vh' }}></Box>
            </Box>
            <FooterWork2 />
        </>
    )
}

export default Work2page2;