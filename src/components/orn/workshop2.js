import Navbar from "./navbar";
import NavbarWork2 from "./navbarWork2";
import FooterWork2 from "./footerWork2"
import styles from 'src/styles/orn/work2page1.module.css'
import {
    Box,
    Button
} from "@mui/material";
import HexagonSharpIcon from '@mui/icons-material/HexagonSharp';
import Grid from '@mui/material/Grid';
import { styled } from '@mui/material/styles';
import * as React from 'react';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemText from '@mui/material/ListItemText';

import CheckCircleIcon from '@mui/icons-material/CheckCircle';
import ForumRoundedIcon from '@mui/icons-material/ForumRounded';
import DesignServicesRoundedIcon from '@mui/icons-material/DesignServicesRounded';
import HomeRoundedIcon from '@mui/icons-material/HomeRounded';
import FiberManualRecordIcon from '@mui/icons-material/FiberManualRecord';


const worklist = ['Flexible Time', 'Perfect Work', 'Client Priority']

const Demo = styled('div')(({ theme }) => ({
    backgroundColor: theme.palette.background.paper,
}));

const Workshop2Compo = () => {
    const [dense, setDense] = React.useState(false);
    const [secondary, setSecondary] = React.useState(false);

    return (
        <>
            <Navbar />
            <NavbarWork2 />
            <body className={styles.body}>
                <div className={styles.firstbg}>
                    <Box classname={styles.welcomeBox}
                        m={3}>
                        <div className={styles.welcome}>WELCOME TO HOMECO</div>
                        <div className={styles.slogan}>BUILD YOUR ELEGAN DREAM HOME INTERIOR</div>
                        <div className={styles.sloganInfo}>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</div>
                        <Button sx={{ backgroundColor: '#3A4D39', color: 'white', borderRadius: '0' }}>
                            OUR PROJECT
                        </Button>
                    </Box>
                    <Box className={styles.hexa}
                        sx={{ width: '50%', display: 'flex', justifyContent: 'center' }} >
                        <HexagonSharpIcon sx={{ fontSize: '400px', fill: '#ECE3CE' }} />
                    </Box>
                </div>
                <Box sx={{ display: 'flex', justifyContent: 'center', transform: 'translate(0,-70px)', color: '#FFF' }}>
                    <Box sx={{ width: '360px', backgroundColor: '#739072', padding: '1.5rem' }}>
                        <div><b>HOME DESIGN CONSULTATION</b></div><br />
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</div>
                    </Box>
                    <Box sx={{ width: '360px', backgroundColor: '#ECE3CE', color: '#4F6F52', padding: '1.5rem' }}>
                        <div><b>HOME DESIGN 3D 2D INTERIOR SERVICE</b></div><br />
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</div>
                    </Box>
                    <Box sx={{ width: '360px', backgroundColor: '#739072', padding: '1.5rem' }}>
                        <div><b>HOME DESIGN CONSULTATION</b></div><br />
                        <div>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</div>
                    </Box>
                </Box>
                <Box sx={{ display: 'flex' }}>
                    <Box sx={{ width: '50%' }}>
                        <Box sx={{ backgroundColor: '#739072', height: '80vh' }}
                            m={3}>

                        </Box>
                    </Box>
                    <Box sx={{ width: '50%' }}>
                        <Box sx={{ height: '80vh', overflow: 'auto' }}
                            m={3}>
                            <Box sx={{ fontWeight: 'bold', color: '#4F6F52' }}>WHO WE ARE</Box>
                            <Box sx={{ fontWeight: '900', fontSize: '30px' }}>WE ARE PERFECT TEAM FOR HOME INTERIOR DECORATION</Box>
                            <Box mt={1}
                                sx={{ color: '#6E6E6E' }}>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                                Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.</Box>
                            <Box sx={{ display: 'flex', padding: '1rem', color: '#6E6E6E' }}>
                                <Grid item
                                    xs={12}
                                    md={6}
                                    mr={20}>
                                    <Demo>
                                        <List dense={dense}>
                                            {worklist.map((item) => (
                                                <ListItem key={item}
                                                    sx={{ color: '#739072' }}>
                                                    <ListItemIcon>
                                                        <CheckCircleIcon sx={{ color: '#739072' }} />
                                                    </ListItemIcon>
                                                    <ListItemText
                                                        primary={item}
                                                        secondary={secondary ? 'Secondary text' : null}
                                                    />
                                                </ListItem>
                                            )
                                            )}
                                        </List>
                                    </Demo>
                                </Grid>
                                <Grid item
                                    xs={12}
                                    md={6}>
                                    <Demo>
                                        <List dense={dense}>
                                            {worklist.map((item) => (
                                                <ListItem key={item}
                                                    sx={{ color: '#739072' }}>
                                                    <ListItemIcon>
                                                        <CheckCircleIcon sx={{ color: '#739072' }} />
                                                    </ListItemIcon>
                                                    <ListItemText
                                                        primary={item}
                                                        secondary={secondary ? 'Secondary text' : null}
                                                    />
                                                </ListItem>
                                            )
                                            )}
                                        </List>
                                    </Demo>
                                </Grid>
                            </Box>
                            <Box sx={{ display: 'flex', justifyContent: 'space-between', padding: '1rem' }}>
                                <div>
                                    <Box sx={{ fontSize: '30px', display: 'flex', justifyContent: 'center', color: '#739072', fontWeight: 'bold' }}>
                                        15Y

                                    </Box>
                                    <div>Experience</div>
                                </div>
                                <div>
                                    <Box sx={{ fontSize: '30px', display: 'flex', justifyContent: 'center', color: '#739072', fontWeight: 'bold' }}>
                                        25+
                                    </Box>
                                    <div>Best Team</div>
                                </div>
                                <div>
                                    <Box sx={{ fontSize: '30px', display: 'flex', justifyContent: 'center', color: '#739072', fontWeight: 'bold' }}>
                                        500+
                                    </Box>
                                    <div>Total Client</div>
                                </div>
                            </Box>

                        </Box>

                    </Box>
                </Box >
                <Box m={3}>
                    <Box sx={{ fontWeight: 'bold', color: '#4F6F52' }}>HOW WE WORK</Box>
                    <div style={{ fontSize: '30px', fontWeight: '900', marginTop: '1rem' }} >OUR WORK PROCEDURE</div>
                </Box>
                <Box m={3}
                    sx={{ display: 'flex', justifyContent: 'center' }} >
                    <Box className={styles.boxDesign}>
                        <ForumRoundedIcon sx={{ fontSize: '40px', color: '#6E6E6E' }} />
                        <Box sx={{ fontSize: '20px', fontWeight: 'bold' }}
                            mt={1}>CLIENT DESIGN CONSULTATION</Box>
                        <Box mt={1}
                            sx={{ color: '#6E6E6E' }}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                        </Box>
                    </Box>
                    <Box className={styles.boxDesign}
                        sx={{ backgroundColor: '#4F6F52', color: 'white' }}>
                        <DesignServicesRoundedIcon sx={{ fontSize: '40px', color: 'white' }} />
                        <Box sx={{ fontSize: '20px', fontWeight: 'bold' }}
                            mt={1}>PROTOTYPING HOME DESIGN</Box>
                        <Box mt={1}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                        </Box>
                    </Box>
                    <Box className={styles.boxDesign}>
                        <HomeRoundedIcon sx={{ fontSize: '40px', color: '#6E6E6E' }} />
                        <Box sx={{ fontSize: '20px', fontWeight: 'bold' }}
                            mt={1}>PROCESSING TO DESIGN HOME</Box>
                        <Box mt={1}
                            sx={{ color: '#6E6E6E' }}>
                            Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                        </Box>

                    </Box>
                </Box>
                <Box sx={{ display: 'flex', width: '150vw', transform: 'translate(-150px,0)' }}>
                    <div className={styles.colorBox}></div>
                    <div className={styles.colorBox}></div>
                    <div className={styles.colorBox}></div>
                    <div className={styles.colorBox}></div>
                    <div className={styles.colorBox}></div>
                </Box>

                <Box
                    sx={{ display: 'flex', alignItems: 'center' }}>
                    <Box sx={{ width: '50%' }}
                        m={3}>
                        <Box sx={{ fontWeight: 'bold', color: '#4F6F52' }}>PERFECT PARTNER</Box>

                        <div style={{ fontSize: '30px', fontWeight: '900', marginTop: '1rem' }} >WE HAVE PRIORITY FOR CAN CREATE DREAM HOME DESIGN</div>
                        <Box mt={1}
                            sx={{ color: '#6E6E6E' }}>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                        </Box>
                        <Button sx={{ backgroundColor: '#3A4D39', color: 'white', borderRadius: '0', marginTop: '1rem' }}
                        >
                            PORTFOLIO
                        </Button>
                    </Box>
                    <Box sx={{ width: '50%', height: '80vh' }}
                        m={3}>
                        <div className={styles.threeBox1}></div>
                        <div className={styles.threeBox2}></div>
                        <div className={styles.threeBox3}></div>
                    </Box>
                </Box>
                <Box sx={{ display: 'flex' }}>
                    <Box sx={{ width: '50%', backgroundColor: '#739072' }}
                        m={3}>

                    </Box>
                    <Box sx={{ width: '50%' }}
                        m={3}>
                        <Box sx={{ fontWeight: 'bold', color: '#4F6F52' }}>TRUST US NOW</Box>
                        <div style={{ fontSize: '30px', fontWeight: '900', marginTop: '1rem' }} >WHY CHOICE OUR HOME DESIGN INTERIOR SERVICES</div>
                        <Box mt={1}
                            sx={{ color: '#6E6E6E' }}>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                        </Box>
                        <Box sx={{ display: 'flex', padding: '1rem', color: '#6E6E6E' }}>
                            <Grid item
                                xs={12}
                                md={6}
                                mr={20}>
                                <Demo>
                                    <List dense={dense}>
                                        {worklist.map((item) => (
                                            <ListItem key={item}
                                                sx={{ color: '#739072' }}>
                                                <ListItemIcon>
                                                    <CheckCircleIcon sx={{ color: '#739072' }} />
                                                </ListItemIcon>
                                                <ListItemText
                                                    primary={item}
                                                    secondary={secondary ? 'Secondary text' : null}
                                                />
                                            </ListItem>
                                        )
                                        )}
                                    </List>
                                </Demo>
                            </Grid>
                            <Grid item
                                xs={12}
                                md={6}>
                                <Demo>
                                    <List dense={dense}>
                                        {worklist.map((item) => (
                                            <ListItem key={item}
                                                sx={{ color: '#739072' }}>
                                                <ListItemIcon>
                                                    <CheckCircleIcon sx={{ color: '#739072' }} />
                                                </ListItemIcon>
                                                <ListItemText
                                                    primary={item}
                                                    secondary={secondary ? 'Secondary text' : null}
                                                />
                                            </ListItem>
                                        )
                                        )}
                                    </List>
                                </Demo>
                            </Grid>
                        </Box>
                    </Box>
                </Box>
                <Box sx={{ display: 'flex' }}>
                    <Box sx={{ width: '50%' }}
                        m={3}>
                        <Box sx={{ fontWeight: 'bold', color: '#4F6F52' }}>CLIENTS FEEDBACK</Box>
                        <div style={{ fontSize: '30px', fontWeight: '900', marginTop: '1rem' }} >OUR TESTIMONIAL FROM BEST CLIENTS</div>
                        <Box mt={1}
                            sx={{ color: '#6E6E6E' }}>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                        </Box>
                    </Box>
                    <Box sx={{ width: '50%', border: '1px solid #4F6F52', padding: '1rem' }}
                        m={3}>
                        <Box mt={1}
                            sx={{ color: '#6E6E6E' }}>
                            Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.
                        </Box>
                        <Box sx={{ display: 'flex', alignItems: 'center' }}>
                            <FiberManualRecordIcon sx={{ fontSize: '80px', color: '#4F6F52' }} />
                            <div>
                                <p style={{ fontWeight: 'bold' }}>JOHN DE</p>
                                <p style={{ color: '#6E6E6E' }}>Art Director</p>

                            </div>
                        </Box>
                    </Box>

                </Box>
                <FooterWork2 />
            </body>

        </>
    )
}
export default Workshop2Compo;