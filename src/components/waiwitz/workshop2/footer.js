const { Box, Button, Typography, Container, Grid } = require("@mui/material")
import FacebookIcon from '@mui/icons-material/Facebook';
import TwitterIcon from '@mui/icons-material/Twitter';
import InstagramIcon from '@mui/icons-material/Instagram';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
import ChevronLeftRoundedIcon from '@mui/icons-material/ChevronLeftRounded';
import LocationOnRoundedIcon from '@mui/icons-material/LocationOnRounded';
import EmailIcon from '@mui/icons-material/Email';
import TextField from '@mui/material/TextField';

const Footer = () => {
    return (
        <>
            <footer style={{}}>
                <Container maxWidth='xl' sx={{ transform: 'translateY(150px)' }}>
                    <Box bgcolor={'white'}
                        height={'fit-content'}
                        sx={{ transform: 'translateY(80px)', margin:{xs: '0 2em' , md: '0 8em'} }}
                        display={'flex'} justifyContent={'space-evenly'} alignItems={'center'} flexWrap={'wrap'}
                        padding={1}
                    >
                        <div style={{ width: '150px', height: '100px', overflow: 'hidden' }}>
                            <img src="https://1000logos.net/wp-content/uploads/2017/03/Mastercard-logo.png" alt="" width={'80%'} style={{ objectFit: 'cover', filter: 'grayscale(100%)' }} />
                        </div>
                        <div style={{ width: '150px', height: '100px', overflow: 'hidden' }}>
                            <img src="https://media.discordapp.net/attachments/1048854301142958141/1175756689207738498/image.png?ex=656c63bf&is=6559eebf&hm=5cc11dbade269ae84565f1c8d17d9209a0e68685a5f01247f807ab03d4b254cf&=&width=2519&height=1245" alt="" width={'100%'} style={{ objectFit: 'cover', filter: 'grayscale(100%)' }} />
                        </div>
                        <div style={{ width: '150px', height: '100px', overflow: 'hidden' }}>
                            <img src="https://fashioninfluencer.sitemaya.com/wp-content/uploads/sites/167/2021/04/logo-13.svg" alt="" width={'100%'} style={{ objectFit: 'cover' }} />
                        </div>
                        <div style={{ width: '150px', height: '100px', overflow: 'hidden' }}>
                            <img src="https://fashioninfluencer.sitemaya.com/wp-content/uploads/sites/167/2021/04/logo-14.svg" alt="" width={'100%'} style={{ objectFit: 'cover' }} />
                        </div>
                    </Box>
                    <Box bgcolor={'#757575'} sx={{padding: {xs: '8em 2em 2em', md: '8em 8em 2em'}}}>
                        <Typography variant="h1" color={'white'} sx={{ fontWeight: '1000' }}>LETS CHANGE YOUR OWN HOME <br /> INTERIOR DESIGN NOW</Typography>
                        <Button sx={{
                            borderRadius: '0',
                            marginTop: '1em', backgroundColor: '#1B1717',
                            width: 'fit-content', color: '#FFFFFF', padding: '1.5em 4em'
                        }}>
                            PORTFOLIO
                        </Button>
                    </Box>
                </Container>
                <Box bgcolor={'#1B1717'} height={'fit-content'} padding={'200px 0 20px'}>
                    <Container maxWidth='xl'>
                        <Grid container justifyContent={'space-between'} columns={{ xs: 4, sm: 8, md: 12 }} marginBottom={'3em'}>
                            <Grid item xs={4} color={'white'} marginTop={2}>
                                <Typography variant="h4" fontWeight={'bolder'} gutterBottom>INFORMATION</Typography>
                                <Typography variant="ิิbody1">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.</Typography>
                                <Box color={'#757575'} margin={'1em 0'}>
                                    <FacebookIcon sx={{ marginRight: '10px' }} />
                                    <TwitterIcon sx={{ marginRight: '10px' }} />
                                    <InstagramIcon sx={{ marginRight: '10px' }} />
                                    <LinkedInIcon sx={{ marginRight: '10px' }} />
                                </Box>
                            </Grid>
                            <Grid item display={'flex'} xs={6} color={'white'} flexWrap={'wrap'}>
                                <Box marginRight={4} marginTop={2}>
                                    <Typography variant="h4" fontWeight={'bolder'} gutterBottom>NAVIGATION</Typography>
                                    <Typography gutterBottom><ChevronLeftRoundedIcon />Homepage</Typography>
                                    <Typography gutterBottom><ChevronLeftRoundedIcon />About Us</Typography>
                                    <Typography gutterBottom><ChevronLeftRoundedIcon />Services</Typography>
                                    <Typography gutterBottom><ChevronLeftRoundedIcon />Project</Typography>
                                </Box>
                                <Box marginTop={2}>
                                    <Typography variant="h4" fontWeight={'bolder'} gutterBottom>CONTACT US</Typography>
                                    <Typography gutterBottom><LocationOnRoundedIcon /> Lumbung Hidup East Java</Typography>
                                    <Typography gutterBottom><EmailIcon /> Hello@Homco.com</Typography>
                                    <TextField label="Email Address" variant="filled" sx={{ marginTop: '10px', width: '100%' }} />
                                    <Button sx={{
                                        borderRadius: '0',
                                        marginTop: '1em', backgroundColor: '#757575',
                                        width: 'fit-content', color: '#FFFFFF', padding: '1.5em 4em'
                                    }}>
                                        SUBSCRIBE
                                    </Button>
                                </Box>
                            </Grid>
                        </Grid>
                        <Grid container padding={'10px 0'} justifyContent={'space-between'} columns={{ xs: 4, sm: 8, md: 12 }} color={'white'} borderTop={'solid 1px'} textTransform={'uppercase'}>
                            <Typography fontWeight={'bold'}>Allright Reserved - Homco Interior</Typography>
                            <ul style={{ display: 'flex', justifyContent: 'space-between', fontWeight: 'bold' }}>
                                <li>Disclaimer</li>
                                <li>Privacy Policy</li>
                                <li>Term Of Use</li>
                            </ul>
                        </Grid>
                    </Container>
                </Box>
            </footer>
        </>

    )
}

export default Footer;