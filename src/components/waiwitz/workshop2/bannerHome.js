import { Button, Grid, ListItem, ListItemButton, Typography } from "@mui/material";
import { Box, Container } from "@mui/system";
import styles from "src/styles/waiwitzStyles.module.css";
import HexagonIcon from '@mui/icons-material/Hexagon';

const BannerHome = () => {
    return (
        <>
            <Box sx={{
                width: '100%', height: 'fit-content',
                backgroundColor: "#989494", padding: '14em 2em 10em'
            }}>
                <Container maxWidth='xl'>
                    <Grid container spacing={3} justifyContent="space-between" alignItems='center' columns={{ xs: 4, sm: 8, md: 12 }}>
                        <Grid item xs={5} sm={7}>
                            <Typography sx={{ color: "#5f5f5f", fontWeight: 'bold', marginBottom: "1em" }}>
                                WELCOME TO HOMECO
                            </Typography>
                            <Box>
                                <Typography className={styles.ws2TitleBanner} sx={{ fontSize: {sm: '40px'}}}>
                                    BUILD YOUR <br/>
                                    ELEGAN DREAM <br />
                                    HOME INTERIOR
                                </Typography>
                                <Typography variant="p" color={'white'}>
                                    Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut elit tellus, luctus nec ullamcorper mattis, pulvinar dapibus leo.
                                </Typography>
                            </Box>
                            <Button sx={{ borderRadius: '0',
                                marginTop: '1em', backgroundColor: '#5f5f5f',
                                width: 'fit-content', color: 'white', padding: '15px 25px'
                            }}>
                                OUR PROJECT
                            </Button>
                        </Grid>
                        <Grid item xs={5} sx={{ display: {xs: 'none' ,md: 'block' } }}>
                            <HexagonIcon sx={{ fontSize: '450px', color: '#c4c4c4' }}>

                            </HexagonIcon>
                        </Grid>
                    </Grid>
                </Container>
            </Box>
        </>
    )
}

export default BannerHome;