import * as React from 'react';
import PropTypes from 'prop-types';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import CssBaseline from '@mui/material/CssBaseline';
import Divider from '@mui/material/Divider';
import Drawer from '@mui/material/Drawer';
import IconButton from '@mui/material/IconButton';
import List from '@mui/material/List';
import ListItem from '@mui/material/ListItem';
import ListItemButton from '@mui/material/ListItemButton';
import ListItemText from '@mui/material/ListItemText';
import MenuIcon from '@mui/icons-material/Menu';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { Container, Link, Menu, MenuItem } from '@mui/material';
import { useRouter } from 'next/router';
import ViewInArIcon from '@mui/icons-material/ViewInAr';
import ExpandMoreRoundedIcon from '@mui/icons-material/ExpandMoreRounded';
import Image from 'next/image';
import Head from 'next/head';
const drawerWidth = 240;
const navItems = [
    {
        name: 'Home',
        link: '/workShop/waiwitz/workshop2',
        dropdown: false

    },
    {
        name: 'About us',
        link: '/workShop/waiwitz/workshop2/aboutUs',
        dropdown: false
    },
    {
        name: 'Our Services',
        link: '',
        dropdown: false,
    },
    {
        name: 'Our Project',
        link: '',
        dropdown: true,
        dropdownList: ['PROJECT']

    },
    {
        name: 'portfolio',
        link: '/workShop/waiwitz/workshop2/error',
        dropdown: false
    },
    {
        name: 'Pages',
        link: '',
        dropdown: true,
        dropdownList: ['FAQ', 'BLOG', 'Single Post']
    },

];

function Navbar(props) {
    const router = useRouter();

    const [anchorEl1, setAnchorEl1] = React.useState(null);
    const [anchorEl2, setAnchorEl2] = React.useState(null);

    const open1 = Boolean(anchorEl1);
    const open2 = Boolean(anchorEl2);

    const handleClick1 = (event, index) => {
        setAnchorEl1(event.currentTarget);
    };
    const handleClick2 = (event, index) => {
        setAnchorEl2(event.currentTarget);
    };
    const handleClose1 = () => {
        setAnchorEl1(null);
    };
    const handleClose2 = () => {
        setAnchorEl2(null);
    };
    const { window } = props;
    const [mobileOpen, setMobileOpen] = React.useState(false);

    const handleDrawerToggle = () => {
        setMobileOpen((prevState) => !prevState);
    };

    const drawer = (
        <Box onClick={handleDrawerToggle} sx={{ textAlign: 'center' }}>
            <Typography variant="h6" sx={{ my: 2 }}>
                HOMCO
            </Typography>
            <Divider />
            <List>
                {navItems.map((item) => (
                    <ListItem key={item.name} disablePadding>
                        <ListItemButton sx={{ textAlign: 'center' }}>
                            <ListItemText primary={item.name} />
                        </ListItemButton>
                    </ListItem>
                ))}
            </List>
        </Box>
    );

    const container = window !== undefined ? () => window().document.body : undefined;

    return (
        <>
            <Head>
                <title>Workshop 2</title>
            </Head>
            <Box sx={{ display: 'flex' }}>
                <AppBar component="nav" sx={{ backgroundColor: '#989494', borderBottom: '1px solid #c0c0c0' }}>
                    <Container maxWidth='xl'>
                        <Toolbar variant="regular" style={{ padding: '0' }}>
                            <Box sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block' } }}>
                                <Image src={'/Logo Homco 1.png'} width={200} height={100} />
                            </Box>
                            {/* <Typography
                            variant="h4"
                            component="div"
                            sx={{ flexGrow: 1, display: { xs: 'none', sm: 'block', color: '#eeeeee' } }}
                        >
                            <ViewInArIcon sx={{ fontSize: '50px', margin: '10px', transform: "translateY(-5px)", color: '#414141' }} />
                            HOMCO
                        </Typography> */}
                            <Box sx={{ display: { xs: 'none', sm: 'none', md: 'flex' }, height: '100%' }}>
                                <Link href={'/workShop/waiwitz/workshop2/'} underline='none'>
                                    <ListItemButton sx={{ height: '123px', color: '#fff', textTransform: 'uppercase', backgroundColor: router.pathname == '/workShop/waiwitz/workshop2' ? '#5c5c5c' : '' }}>
                                        Home
                                    </ListItemButton>
                                </Link>
                                <Link href={'/workShop/waiwitz/workshop2/aboutUs'} underline='none'>
                                    <ListItemButton sx={{ height: '123px', color: '#fff', textTransform: 'uppercase', backgroundColor: router.pathname == '/workShop/waiwitz/workshop2/aboutUs' ? '#5c5c5c' : '' }}>
                                        About us
                                    </ListItemButton>
                                </Link>
                                <Link href={'/workShop/waiwitz/workshop2/error'} underline='none'>
                                    <ListItemButton sx={{ height: '123px', color: '#fff', textTransform: 'uppercase', backgroundColor: router.pathname == '/workShop/waiwitz/workshop2/error' ? '#5c5c5c' : '' }}>
                                        Our Services
                                    </ListItemButton>
                                </Link>
                                <Box>
                                    <ListItemButton sx={{ height: '123px', color: '#fff', textTransform: 'uppercase', backgroundColor: router.pathname == '#' ? '#5c5c5c' : '' }}
                                        onClick={handleClick1}>
                                        Our Projects
                                        <ExpandMoreRoundedIcon />
                                    </ListItemButton>
                                    <Menu
                                        id={'Our Projects'}
                                        anchorEl={anchorEl1}
                                        open={open1}
                                        onClose={handleClose1}
                                        MenuListProps={{
                                            'aria-labelledby': 'basic-button',
                                        }}
                                    >
                                        <MenuItem onClick={handleClose1}>PROJECT</MenuItem>
                                    </Menu>
                                </Box>
                                <Link href={'#'} underline='none'>
                                    <ListItemButton sx={{ height: '123px', color: '#fff', textTransform: 'uppercase', backgroundColor: router.pathname == '#' ? '#5c5c5c' : '' }}>
                                        portfolio
                                    </ListItemButton>
                                </Link>
                                <Box>
                                    <ListItemButton sx={{ height: '123px', color: '#fff', textTransform: 'uppercase', backgroundColor: router.pathname == '#' ? '#5c5c5c' : '' }}
                                        onClick={handleClick2}>
                                        Pages
                                        <ExpandMoreRoundedIcon />
                                    </ListItemButton>
                                    <Menu
                                        id={'Pages'}
                                        anchorEl={anchorEl2}
                                        open={open2}
                                        onClose={handleClose2}
                                        MenuListProps={{
                                            'aria-labelledby': 'basic-button',
                                        }}
                                    >
                                        <MenuItem onClick={handleClose2}>FAQ</MenuItem>
                                        <MenuItem onClick={handleClose2}>BLOG</MenuItem>
                                        <MenuItem onClick={handleClose2}>SINGLE POST</MenuItem>
                                    </Menu>
                                </Box>
                            </Box>
                            <IconButton
                                color="inherit"
                                aria-label="open drawer"
                                edge="start"
                                onClick={handleDrawerToggle}
                                sx={{ mr: 2, display: { md: 'none' } }}
                            >
                                <MenuIcon />
                            </IconButton>
                        </Toolbar>
                    </Container>
                </AppBar>
                <nav>
                    <Drawer
                        container={container}
                        variant="temporary"
                        open={mobileOpen}
                        onClose={handleDrawerToggle}
                        ModalProps={{
                            keepMounted: true, // Better open performance on mobile.
                        }}
                        sx={{
                            display: { sm: 'block', md: 'none' },
                            '& .MuiDrawer-paper': { boxSizing: 'border-box', width: drawerWidth },
                        }}
                    >
                        {drawer}
                    </Drawer>
                </nav>
            </Box>
        </>
    );
}

Navbar.propTypes = {
    /**
     * Injected by the documentation to work in an iframe.
     * You won't need it on your project.
     */
    window: PropTypes.func,
};

export default Navbar;
