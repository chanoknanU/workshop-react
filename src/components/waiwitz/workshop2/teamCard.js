import { Box, Card, CardContent, CardMedia, Typography, Link } from "@mui/material";
import FacebookOutlinedIcon from '@mui/icons-material/FacebookOutlined';
import TwitterIcon from '@mui/icons-material/Twitter';
import LinkedInIcon from '@mui/icons-material/LinkedIn';
const Team = ({ position, name, index }) => {
    let findOdd = index % 2;
    return (
        <Card sx={{ borderRadius: '0', border: '1px solid #DADADA' }}>
            <CardMedia sx={{ bgcolor: '#C4C4C4', height: '540px' }} src="#"/>
            <CardContent sx={{ textAlign: 'center', bgcolor: findOdd !== 0 ? '#fff' : '#1B1717' }}>
                <Box color={findOdd !== 0 ? 'black' : 'white'}>
                    <Typography variant="caption">{position}</Typography>
                    <Typography variant="h5" gutterBottom>{name}</Typography>
                </Box>
                <Box>
                    <Link color={'#757575'} href='#'>
                        <FacebookOutlinedIcon sx={{ margin: '0 5px' }} />
                    </Link>
                    <Link color={'#757575'} href='#'>
                        <TwitterIcon sx={{ margin: '0 5px' }} />
                    </Link>
                    <Link color={'#757575'} href='#'>
                        <LinkedInIcon sx={{ margin: '0 5px' }} />
                    </Link>
                </Box>
            </CardContent>
        </Card>
    )
}

export default Team;