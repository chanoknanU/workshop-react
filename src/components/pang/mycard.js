import React from "react";
import Typography from "@mui/material/Typography";
import Avatar from "@mui/material/Avatar";

const About = () => {
  return (
    <div style={{ maxWidth: "800px", margin: "auto", padding: "10px" }}>
      <Avatar
        alt="Profile Image"
        src="/me.jpg"
        sx={{ width: 200, height: 200, margin: "auto", backgroundColor: "#000000" }}
      />
      <div style={{ maxWidth: "1000px", margin: "20px", padding: "10px", textAlign: "left" }}>
        <div style={{ backgroundColor: "#ede7f6", padding: "20px", borderRadius: "8px" }}>
          <Typography
            variant="h3"
            fontWeight="bold"
            mb={2}
            mt={1}
            sx={{
              textAlign: "center",
              letterSpacing: 5,
              border: 1,
              padding: "10px",
              backgroundColor: "#ce93d8",
              color: "#fff",
              borderRadius: "8px",
            }}
          >
            About Me
          </Typography>
          <Typography variant="body1" align="center" paragraph>
            Hi, I'm Hathachanok Sirikul, but you can call me Pang. I entered this world on January
            1, 2540. Currently, I'm honing my skills as a frontend developer through an internship
            on P'aui's team. We're diving into the world of React and Next.js, and I'm committed to
            giving my best, even if I'm not an expert yet. When I'm not immersed in code, you'll
            find me indulging in my favorite pastimes. I love diving into Valorant and TFT
            (Teamfight Tactics) during my free time. And, of course, I'm a dedicated fan of Taylor
            Swift—her music is a constant companion. Nice to meet you!
          </Typography>
          <div>
            <Typography variant="body1" paragraph>
              <strong>Skills:</strong>
            </Typography>
            <ul style={{ listStyleType: "none", padding: 0, marginLeft: "0" }}>
              <li style={{ marginBottom: "8px", display: "flex", alignItems: "center" }}>
                <span style={{ marginRight: "8px", width: "24px", textAlign: "center" }}>🚀</span>
                <Typography variant="body2">React basic</Typography>
              </li>
              <li style={{ marginBottom: "8px", display: "flex", alignItems: "center" }}>
                <span style={{ marginRight: "8px", width: "24px", textAlign: "center" }}>🎨</span>
                <Typography variant="body2">Tailwind basic</Typography>
              </li>
              <li style={{ marginBottom: "8px", display: "flex", alignItems: "center" }}>
                <span style={{ marginRight: "8px", width: "24px", textAlign: "center" }}>🌐</span>
                <Typography variant="body2">UX/UI</Typography>
              </li>
            </ul>

            <Typography variant="body1" paragraph>
              <strong>My hobbies:</strong>
            </Typography>
            <ul style={{ listStyleType: "none", padding: 0, marginLeft: "0" }}>
              <li style={{ marginBottom: "8px", display: "flex", alignItems: "center" }}>
                <span style={{ marginRight: "8px", width: "24px", textAlign: "center" }}>📚</span>
                <Typography variant="body2">Reading inspirational books</Typography>
              </li>
              <li style={{ marginBottom: "8px", display: "flex", alignItems: "center" }}>
                <span style={{ marginRight: "8px", width: "24px", textAlign: "center" }}>🎮</span>
                <Typography variant="body2">Being a Valorant and TFT enthusiast</Typography>
              </li>
            </ul>
          </div>

          <iframe
            style={{ borderRadius: "12px", marginTop: "20px" }}
            src="https://open.spotify.com/embed/track/59KOoHFcw5XfICnO57holu?utm_source=generator"
            width="100%"
            height="352"
            allowFullScreen=""
            allow="autoplay; clipboard-write; encrypted-media; fullscreen; picture-in-picture"
            loading="lazy"
          ></iframe>
        </div>
      </div>
    </div>
  );
};

export default About;
