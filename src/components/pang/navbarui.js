import React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Link from "next/link";
import WidgetsIcon from "@mui/icons-material/Widgets";
import MenuIcon from "@mui/icons-material/Menu";
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';


const Navbarui = () => {
  return (
    <AppBar position="static" style={{ backgroundColor: "#9e9e9e" ,height: "100px", paddingTop: "15px",  borderBottom: "1px solid #FFFFFF"}}>
      <Toolbar style={{ justifyContent: "space-between" }}>
        <div style={{ display: "flex", alignItems: "center", paddingLeft: "20px"}}>
          <WidgetsIcon
            style={{
              width: "70px",
              height: "70px",
              fontWeight: "bold",
              padding: "10px",
              borderRadius: "8px",
              boxSizing: "border-box",
            }}
          />
          <Typography variant="h3" fontWeight="bold" style={{ marginLeft: "5px" }}>
            HOMCO
          </Typography>
        </div>
        {/* <Link href="/workShop/meldyjuju">
          <Typography variant="h6">Welcome to hathaichanok workshop</Typography>
        </Link> */}
        <div style={{ display: "flex", alignItems: "right", marginLeft: "100px" }}>
        <Typography variant="h10"    fontWeight="semibold" style={{ marginLeft: "50px" , paddingTop:"5px", paddingLeft:"250px"}}> HOME</Typography>
        <Typography variant="h10" fontWeight="semibold" style={{ marginLeft: "30px",marginBottom: "5px", paddingTop:"5px"}}>ABOUT US</Typography>
        <Typography variant="h10" fontWeight="semibold" style={{ marginLeft: "30px",marginBottom: "5px" , paddingTop:"5px"}}>OUR SERVICES </Typography>
        <Typography variant="h10" fontWeight="semibold" style={{ marginLeft: "30px",marginBottom: "5px", paddingTop:"5px" }}>OUR PROJECTS<KeyboardArrowDownIcon style={{ marginBottom: "5px" }}/></Typography>
        <Typography variant="h10" fontWeight="semibold" style={{ marginLeft: "30px",marginBottom: "5px", paddingTop:"5px"}}>PORTFOLIO </Typography>
        <Typography variant="h10" fontWeight="semibold" style={{ marginLeft: "30px", marginBottom:"5px", paddingTop:"5px"}}>PAGES <KeyboardArrowDownIcon  style={{ marginBottom: "5px" }}/></Typography>
        </div>
        <div style={{marginRight: "20px"}}>
        <MenuIcon
          style={{
            width: "70px",
            height: "70px",
            fontWeight: "bold",
            padding: "10px",
            borderRadius: "8px",
            boxSizing: "border-box",
          }}
        /></div>
      </Toolbar>
    </AppBar>
  );
};

export default Navbarui;


