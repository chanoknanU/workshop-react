import { Box, Button, Typography } from "@mui/material";
import React from "react";

const Body3 = () => {
  return (
    <Box
      bgcolor="#00796b"
      minHeight="330px"
      paddingX={{ xs: 2, sm: 4, md: 6 }} 
      paddingTop={{ xs: 4, sm: 6, md: 8 }} 
      paddingBottom={{ xs: 4, sm: 6, md: 4}} 
    >
      <div style={{ paddingTop: "30px" }}>
        <Typography
          variant="h4"
          fontSize={{ xs: 30, md: 50 }} 
          fontWeight="bold"
          color="#FFFFFF"
          textAlign="left"
          letterSpacing={2}
          marginLeft={{ xs: 2, md: 4 }} 
          marginBottom={{ xs: 1, md: 1 }} 
        >
          LET'S CHANGE YOUR OWN HOME
        </Typography>
        <Typography
          variant="h4"
          fontSize={{ xs: 30, md: 50 }} 
          fontWeight="bold"
          color="#FFFFFF"
          textAlign="left"
          letterSpacing={2}
          marginLeft={{ xs: 2, md: 4 }}
          marginBottom={{ xs: 1, md: 1 }} 
        >
          INTERIOR DESIGN NOW
        </Typography>
        <div style={{ paddingBottom: "2px" }}>
          <Button
            variant="contained"
            sx={{
              width: "100%",
              maxWidth: "250px",
              height: "65px",
              fontWeight: "bold",
              backgroundColor: "#000000",
              color: "#FFFFFF",
              marginTop: { xs: 2, md: 0 }, // Responsive top margin
              marginLeft: { xs: 1, md: 4 }, // Responsive left margin
            }}
          >
            CONTACT US
          </Button>
        </div>
      </div>
    </Box>
  );
};

export default Body3;
