import React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import Link from "next/link";

const Navbar = () => {
  return (
    <AppBar position="static" style={{ backgroundColor: "#424242" }}>
      <Toolbar>
      <Link  href="/workShop/meldyjuju">
        <Typography variant="h6">Welcome to hathaichanok workshop</Typography></Link>
        <div>
        </div>
      </Toolbar>
    </AppBar>
  );
};

export default Navbar;
