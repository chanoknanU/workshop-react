import React, { useState } from "react";
import { Button, TextField, Typography } from "@mui/material";
import { createTheme, ThemeProvider } from "@mui/material/styles";

const Login = ({ onLogin }) => {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");

  const theme = createTheme({
    palette: {
      primary: {
        main: "#00bcd4",
        light: "#42a5f5",
        dark: "#1565c0",
        contrastText: '#fff',
      },
    },
  });

  const handleLogin = async () => {
    try {
      console.log("info", "Login request:", {
        method: "POST",
        url: `${process.env.NEXT_PUBLIC_API_URL}/login`,
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username,
          password,
        }),
      });

      if (!password) {
        console.error("error", "Password is required na");
        return;
      }

      if (!username) {
        console.error("error", "Username is required na");
        return;
      }

      const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/login`, {
        method: "POST",
        headers: {
          "Content-Type": "application/json",
        },
        body: JSON.stringify({
          username,
          password,
        }),
      });

      const data = await response.json();

      if (response.ok) {
        console.log("info", "Login successful:", data);
        const jwt = data.accessToken;
        onLogin(jwt);
        localStorage.setItem("jwt", jwt);
      } else {
        console.error("error", "Login failed:", data);
      }
    } catch (error) {
      console.error("error", "An error occurred during login:", error);
    }
  };

  return (
    <ThemeProvider theme={theme}>
      <div>
        <Typography variant="h4" gutterBottom>
          Login
        </Typography>
        <TextField
          label="Username"
          variant="outlined"
          fullWidth
          margin="normal"
          value={username}
          onChange={(e) => setUsername(e.target.value)}
        />
        <TextField
          label="Password"
          type="password"
          variant="outlined"
          fullWidth
          margin="normal"
          value={password}
          onChange={(e) => setPassword(e.target.value)}
        />
        <Button variant="contained" color="primary" onClick={handleLogin}>
          Login
        </Button>
      </div>
    </ThemeProvider>
  );
};

export default Login;
