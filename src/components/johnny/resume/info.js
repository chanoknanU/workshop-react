import { PaperClipIcon } from '@heroicons/react/20/solid'

export default function info() {
    return (
        <div>
            {/* หัวข้อ */}
            <div className=" px-4 sm:px-0">
                <h1 className="text-xl font-bold mb-6">PERSONAL INFORMATION</h1>
            </div>
            {/* เนื้อหา */}
            <div className="flex w-full rounded-lg border border-gray-200 shadow px-4">
                <div className="w-1/2 px-3 pl-1 pr-5 border-r-2 ">
                    <dl className="divide-y divide-gray-300">
                        <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                            <dt className="text-sm font-medium leading-6 text-gray-900">Full name</dt>
                            <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">Natpacan Sribanhad [ณัฐปคัลภ์ ศรีบ้านแฮด]</dd>
                        </div>
                        <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                            <dt className="text-sm font-medium leading-6 text-gray-900">Application for</dt>
                            <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">Frontend Developer</dd>
                        </div>
                        <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                            <dt className="text-sm font-medium leading-6 text-gray-900">University</dt>
                            <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">Bachelor of Science Program in Information Technology Khon Kaen University - 3rd year</dd>
                        </div>
                        <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                            <dt className="text-sm font-medium leading-6 text-gray-900">GPA</dt>
                            <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">3.24</dd>
                        </div>

                        <div className="px-4 py-6 sm:grid sm:grid-cols-3 sm:gap-4 sm:px-0">
                            <dt className="text-sm font-medium leading-6 text-gray-900">Attachments</dt>
                            <dd className="mt-2 text-sm text-gray-900 sm:col-span-2 sm:mt-0">
                                <ul role="list" className="divide-y divide-gray-300 rounded-md border border-gray-200">
                                    <li className="flex items-center justify-between py-4 pl-4 pr-5 text-sm leading-6">
                                        <div className="flex w-0 flex-1 items-center">
                                            <PaperClipIcon className="h-5 w-5 flex-shrink-0 text-gray-300" aria-hidden="true" />
                                            <div className="ml-4 flex min-w-0 flex-1 gap-2">
                                                <span className="truncate font-medium">resume_back_end_developer.pdf</span>
                                                <span className="flex-shrink-0 text-gray-300">2.4mb</span>
                                            </div>
                                        </div>
                                        <div className="ml-4 flex-shrink-0">
                                            <a href="#" className="font-medium text-indigo-600 hover:text-indigo-500">
                                                Download
                                            </a>
                                        </div>
                                    </li>
                                </ul>
                            </dd>
                        </div>
                    </dl>
                </div>

                <div className="w-1/2 px-3 pr-1 pl-5 border-l-2">
                    <dl className="divide-y divide-gray-300">
                    <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:px-0 ">
                            <dt className="text-sm font-medium leading-6 text-gray-900">Address</dt>
                            <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">915/18, Sila, Mueang, Khon kaen ,Thailand</dd>
                        </div>
                        <div className="px-4 py-4 sm:grid sm:grid-cols-3  sm:px-0">
                            <dt className="text-sm font-medium leading-6 text-gray-900">Phone</dt>
                            <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">0933741160</dd>
                        </div>
                        <div className="px-4 py-4 sm:grid sm:grid-cols-3  sm:px-0">
                            <dt className="text-sm font-medium leading-6 text-gray-900">Email</dt>
                            <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">natpacan.sri@gmail.com <p>ㅤ</p></dd>
                        </div>
                        <div className="px-4 py-4 sm:grid sm:grid-cols-3  sm:px-0">
                            <dt className="text-sm font-medium leading-6 text-gray-900">github</dt>
                            <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">github.com/NatpacanSribanhad</dd>
                        </div>
                        <div className="px-4 py-4 sm:grid sm:grid-cols-3 sm:px-0">
                            <dt className="text-sm font-medium leading-6 text-gray-900">LinkedIn</dt>
                            <dd className="mt-1 text-sm leading-6 text-gray-700 sm:col-span-2 sm:mt-0">linkedin.com/in/natpacan-srilinkedin.com/in/natpacan-sri</dd>
                        </div>
                        
                    </dl>
                </div>

            </div>

        </div>
    )
}
