import React from 'react'
import { 
    Container,
} from '@mui/material'
import Navbar from './nav'

export default function layout({children}) {
  return (
    <Container>
        <Navbar/>
        {children}
    </Container>
  )
}
