import React from 'react';
import { Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper, Avatar } from '@mui/material';

const AttrTable = ({ attrData }) => {
  return (
    <TableContainer component={Paper}>
      <Table>
        <TableHead>
          <TableRow>
            <TableCell>ID</TableCell>
            <TableCell>Name</TableCell>
            <TableCell>Detail</TableCell>
            <TableCell>Coverimage</TableCell>
            <TableCell>latitude</TableCell>
            <TableCell>longitude</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
            {attrData.length > 0 ?
                attrData.map((attr) => (
                    <TableRow key={attr.id}>
                      <TableCell>{attr.id}</TableCell>
                      <TableCell>{attr.name}</TableCell>
                      <TableCell>{attr.detail}</TableCell>
                      <TableCell>
                        <img alt={`Avatar-${attr.coverimage}`} src={attr.coverimage} width={100} height={70} />
                      </TableCell>
                      <TableCell>{attr.latitude}</TableCell>
                      <TableCell>{attr.longitude}</TableCell>
                    </TableRow>
                  ))
            :<>no data</>
            }
        </TableBody>
      </Table>
    </TableContainer>
  );
};

export default AttrTable;
