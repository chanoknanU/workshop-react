import React,{useState} from 'react'
import {
    Accordion,
    AccordionDetails,
    AccordionSummary,
    Typography
} from '@mui/material'
import ExpandMoreIcon from '@mui/icons-material/ExpandMore';


export default function accordion({name,api,component,expanded,index}) {

    const [isExpanded, setExpanded] = useState(false);

    const handleChangeAccordion = (panel) => (event, _isExpanded) => {
        setExpanded(_isExpanded ? panel : false);
    };

    return (
        <>
            <Accordion key={index} expanded={isExpanded === `panel${index + 1}`} onChange={handleChangeAccordion(`panel${index + 1}`)}
            // sx={{boxShadow:6,}}
            >
                <AccordionSummary
                    expandIcon={<ExpandMoreIcon />}
                    aria-controls={`panel${index + 1}bh-content`}
                    id={`panel${index + 1}bh-header`}
                >
                    <Typography sx={{ width: '20%', flexShrink: 0, fontWeight: '600', marginRight: '10px' }}>
                        {name}
                    </Typography>
                    <Typography sx={{ widht: "80%", color: 'text.secondary' }}>
                        {api}
                    </Typography>
                </AccordionSummary>
                <AccordionDetails>
                    <Typography>
                        {expanded ? component : ''}
                    </Typography>
                </AccordionDetails>
            </Accordion>
        </>
    )
}
