import React, { useState, useEffect } from 'react';
import { Container, Typography, CircularProgress } from '@mui/material';
import CLayout from '../cLayout';
import UserTable from '../userTable';
import TablePaginationDemo from '../TablePaginationDemo'; 


export default function Pagination() {
    const [userData, setUserData] = useState([]);
    const [loading, setLoading] = useState(true);
    const [currentPage, setCurrentPage] = useState(1);
    const [perPage, setPerPage] = useState(5);
    // const [res,setRes] = useState()


    useEffect(() => {
        const fetchData = async () => {
            try {
                const response = await fetch(`${process.env.NEXT_PUBLIC_API_URL}/users?page=${currentPage}&per_page=${perPage}`);
                const res = await response.json();
                const userData = res.data;
                setUserData(userData);
                        
                console.log("data:", res)
                console.log("user:", userData)
                
                if(userData.length > 0){
                    setLoading(false);
                }
                
            } catch (error) {
                console.error('Error fetching data:', error);
                setLoading(false);
            }
        };

        fetchData();
    }, [loading]);


    const handlePageChange = (event, newPage) => {
        setCurrentPage(newPage);
    };

    const handleRowsPerPageChange = (event) => {
        setPerPage(parseInt(event.target.value, 10));
        setCurrentPage(1);
    };

    return (
        <CLayout title="Pagination">
            {loading ? (
                <CircularProgress />
            ) : (
                <>
                    <TablePaginationDemo
                count={5} // Total number of items, adjust this based on your use case
                page={currentPage - 1}
                onPageChange={handlePageChange}
                rowsPerPage={perPage}
                onRowsPerPageChange={handleRowsPerPageChange}
              />
                    <UserTable userData={userData} />
                </>
            )}
        </CLayout>
    );
}

