import React, { useState, useEffect } from 'react';
import { 
    Container, 
    Typography,
    CircularProgress 
} from '@mui/material';
import CLayout from '../cLayout';
import UserTable from '../userTable';

export default function UserList() {
  const [userData, setUserData] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const fetchData = async () => {
      try {
        const response = await fetch(process.env.NEXT_PUBLIC_API_URL+"/users");
        const data = await response.json();
        setUserData(data);
        console.log("userData:",userData)
        setLoading(false);
      } catch (error) {
        console.error('Error fetching data:', error);
        setLoading(false);
      }
    };

    fetchData();
  }, []);

  return (
    <CLayout title="User List">
        {loading ? (
        <CircularProgress />
      ) : (
        <UserTable userData={userData} />
      )}
    </CLayout>
    // <Container maxWidth="md">
    //   <Typography variant="h5" marginBottom={3}>
    //     User List
    //   </Typography>
    //   {loading ? (
    //     <CircularProgress />
    //   ) : (
    //     <UserTable userData={userData} />
    //   )}
    // </Container>
  );
}
