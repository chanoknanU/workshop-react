import React from 'react'
import Image from 'next/image'
import {
    Container,
    Box,
    Typography,
    Button,
    Grid,
    List,
    ListItem
} from "@mui/material"
import CheckCircleIcon from '@mui/icons-material/CheckCircle';

import imageBlog from '../../../../public/imgJohnny/imageBlog.jpg'

export default function blogSection() {

    const items = [
        {
            name: 'Flexible Time'
        },
        {
            name: 'Flexible Time'
        },
        {
            name: 'Perfect Work'
        },
        {
            name: 'Perfect Work'
        },
        {
            name: 'Client Priority'
        },
        {
            name: 'Client Priority'
        },

    ]

    return (
        <Container maxWidth='xl' sx={{ marginTop: '60px' }}>
            <Grid container
                sx={{

                }}
            >
                <Grid item lg={6}
                    sx={{

                    }}
                >
                    <Image
                        src={imageBlog}

                    />
                </Grid>
                <Grid item lg={6}
                    sx={{
                        padding: '5px 35px',
                    }}
                >
                    <Typography
                        sx={{
                            fontSize: '2.5em',
                            fontWeight: '900',
                            marginBottom: "20px"
                        }}
                    >WE ARE PERFECT TEAM FOR HOME INTERIOR DECORATION</Typography>
                    <Typography
                        sx={{

                        }}
                    >
                        Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. <br />
                        Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt.
                    </Typography>
                    <Grid container
                        sx={{
                            marginTop:'20px'
                        }}
                    >
                        
                            {items.map((item, index) => (
                                <Grid item
                                    md={6}
                                    sx={{
                                    }}
                                >
                                    <ListItem  >
                                        <CheckCircleIcon /> {item.name}
                                    </ListItem>
                                </Grid>
                            ))}
                        
                    </Grid>
                </Grid>
            </Grid>
        </Container>
    )
}
