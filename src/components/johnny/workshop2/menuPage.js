import * as React from 'react';
import Button from '@mui/material/Button';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import PopupState, { bindTrigger, bindMenu } from 'material-ui-popup-state';
import Link from 'next/link'

export default function MenuPopupState() {
    return (
        <PopupState variant="popover" popupId="demo-popup-menu">
            {(popupState) => (
                <React.Fragment>
                    <Button variant="contained" {...bindTrigger(popupState)}
                        sx={{
                            backgroundColor: "#19A7CE",
                            boxShadow: "none",
                            "&:hover": {
                                backgroundColor: "#46b8d7", 
                            },
                        }} >
                        Page &#9660;
                    </Button>
                    <Menu {...bindMenu(popupState)} sx={{}}>
                        <Link href="/workShop/natpacanSri/workshop2/blog"><MenuItem onClick={popupState.close}>blog</MenuItem></Link>
                        <Link href="/workShop/natpacanSri/workshop2/notFound"><MenuItem onClick={popupState.close}>404</MenuItem></Link>
                    </Menu>
                </React.Fragment>
            )}
        </PopupState>
    );
}